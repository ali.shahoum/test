
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <form method="POST" action="<?php echo e(url('admin/article/add')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-group"><br>
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>افزودن مقاله</h2></div>
                    <div class="panel-body">
                        <div class="col-md-11">
                            <label >موضوع</label>
                            <input  name="title" type="text"  class="form-control" required="required"><br>

                            <label >پیش گفتار</label>
                            <textarea  name="description_first" type="text"  id="content" class="form-control " required="required"></textarea><br>
                            <br>
                            <label >توضیحات</label>
                            <textarea  name="description" type="text" id="content2" class="form-control ckeditor" required="required"></textarea><br>

                            <label >عکس</label>
                            <input  name="image" type="file" ><br>
                            <label > URL (در صورت خالی بودن این ایتم نام مقاله جایگزین می شود)</label>
                            <input  name="title_id" type="text" placeholder="به هیچ وجه تکراری نباشد" class="form-control" ><br>

                            <hr>
                            <label >عنوان سئو</label>
                            <input  name="title_seo" type="text"  class="form-control"><br>

                            <label > توضیحات سئو</label>
                            <textarea  name="description_seo" type="text"  class="form-control "></textarea><br>


                            <div class="panel panel-success">
                                <div class="panel-heading"><h2>وضعیت مقاله </h2></div>

                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <select  id="selectbasic" name="status" class="form-control">
                                                <option value="0">در حال ویرایش</option>
                                                <option value="1">در صف  انتشار</option>
                                                <option value="2">منتشر شده</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script type="text/javascript">

    CKEDITOR.replace('content', {
    language: 'fa',
    filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
    filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
    filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
    filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>'
    });

    </script>
    <script type="text/javascript">

    CKEDITOR.replace('content2', {
    language: 'fa',
    filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
    filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
    filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
    filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>'
    });

    </script>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>