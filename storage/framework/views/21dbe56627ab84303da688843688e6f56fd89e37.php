
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form method="POST" action="<?php echo e(URL::action('Admin\ServicesController@postAddLoges')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <br>
                        <input type="hidden" name="kind" value="2"/>
                        <div class="panel panel-info">
                            <div class="panel-heading">افزودن پروژه</div>

                            <div class="panel-body">
                                <div class="col-md-3">

                                    <input placeholder="نام پروژه" name="title" type="text"  class="form-control"><br>
                                </div>
                                
                                <div class="col-md-5">

                                    <input  name="image" type="file" ><br>
                                </div>
                                <div class="col-md-2">
                                    <button id="singlebutton" name="singlebutton" class="btn btn-success">افزودن</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="table-responsive">

                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>
                                    <center>
                                        <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
                                    </center>
                                    <div class="icheckbox_flat-green" style="position: relative;"><input id="check-all" class="flat" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                </th>
                                <th class="column-title" style="display: table-cell;">شماره</th>
                                <th class="column-title" style="display: table-cell;">نام پروژه</th>
                                <th class="column-title" style="display: table-cell;">تصویر پیش فرض</th>

                                <th class="column-title" style="display: table-cell;">عملیات</th>


                            </tr>
                            </thead>

                            <tbody>
                            <?php if(isset($success)): ?>
                                <div class="alert alert-success"> ' با موفقیت حذف شد'</div>
                            <?php endif; ?>

                            <?php if(count(@$project) == 0): ?>
                                <h1> چیزی یافت نشد</h1>
                            <?php else: ?>
                                <?php $__currentLoopData = $project; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <center>
                                                <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                       type="checkbox"
                                                       value="<?php echo e($row->id); ?>"/>

                                            </center>
                                        <td class=" "><?php echo e($key+1); ?></td>

                                        <div class="icheckbox_flat-green" style="position: relative;"><input class="flat" name="table_records" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        </td>
                                        <td class=" "><?php echo e($row->title); ?></td>

                                        <td class=" "> <img style="width: 100px " src="<?php echo e(asset('assets/images/services/'.$row->image)); ?>"></td>

                                        
                                        <td><a href="<?php echo e(url('admin/services/delete/'.$row->id)); ?>" class="btn btn-danger">    <i class="fa fa-trash"></i>  </a>
                                            <a href="<?php echo e(url('admin/services/more_image/'.$row->id)); ?>" class="btn btn-success" title="عکس بیشتر" >تصاویر بیشتر پروژه</a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>


                            </tbody>
                            <center> <?php if(count($project)): ?> <?php echo $project->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script>

        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>