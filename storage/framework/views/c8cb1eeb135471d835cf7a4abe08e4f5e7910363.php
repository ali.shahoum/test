<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $__env->yieldContent('title' , $setting->title); ?></title>
    <?php $__env->startSection('title'); ?>
        <?php echo e($setting->title); ?>

    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('description'); ?>
        <?php echo e(@$setting->description); ?>

    <?php $__env->stopSection(); ?>
    <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
    <link rel="icon"
          <?php if($setting->fav): ?>
          href="<?php echo e(asset('assets/uploads/uploader/setting/medium/'.@$setting->fav)); ?>">
    <?php else: ?>
        href="<?php echo e(asset('assets/site/image/favicon.ico')); ?>" >
    <?php endif; ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169229204-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-169229204-1');
    </script>
    <link href="<?php echo e(asset('assets/site/css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/w3.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/swiper.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/owl.carousel.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/owl.carousel.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/bootstrap.min.css')); ?>" rel="stylesheet" id="bootstrap-css">
    <link href="<?php echo e(asset('assets/site/css/css/classy-nav.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/elegant-icon.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/site/css/css/magnific-popup.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/toastr.css')); ?>">
    <script src="<?php echo e(asset('assets/admin/js/toastr.js')); ?>"></script>



    
    
    <link href="https://fonts.googleapis.com/css?family=Oswald:500" rel="stylesheet">




<!-- Title -->


<!-- Favicon -->

    <!-- Core Stylesheet -->
</head>