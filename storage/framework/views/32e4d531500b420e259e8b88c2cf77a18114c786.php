<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/admin/edit')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>


            <div class="col-md-9 col-md-offset-0" id="app2">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش کاربر</h2></div>

                    <div class="panel-body">
                        <div class="col-md-12">

                <label >نام</label>
                <input value="<?php echo e($user->name); ?>" name="name" type="text" class="form-control"/><br>
                <label >نام خانوادگی</label>
                <input value="<?php echo e($user->family); ?>" name="family" type="text" class="form-control"/><br>
                  <label >ایمیل</label>
                <input value="<?php echo e($user->email); ?>" name="email" type="text" class="form-control"/><br>
                  <label >موبایل</label>
                <input value="<?php echo e($user->mobile); ?>" name="mobile" type="text" class="form-control"/><br>
                 <label >تصویر</label><br>
                            <td><img  style="width: 100px;margin-right: 0px!important;" <?php if($user->image): ?> src="<?php echo e(asset('assets/images/users/'.$user->image)); ?>"<?php else: ?> src="<?php echo e(asset('img/users.png')); ?>"<?php endif; ?> alt="..." class="img-circle profile_img">
                            </td>


                            <input value="<?php echo e($user->image); ?>" name="image" type="file"><br>
                            <div class="panel panel-success">
                                <div class="panel-heading"><h2>سطح دسترسی </h2></div>

                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <div class="col-md-2">
                                                    <input type="radio" name="group[]" value="<?php echo e($group->id); ?>" <?php if(in_array($group->id, $groupsId)): ?> checked="checked" <?php endif; ?>>

                                                    <span class=""><?php echo e($group->name); ?></span>

                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                    <div class="form-group">
                                <label></label>

                            </div>
                <label >رمز</label>
                <input value="" name="password" type="password" class="form-control"/>
                <br>

                <div class="modal-footer">
                    <input value="<?php echo e($user->id); ?>" name="id" type="hidden"/>
                    <input type="submit" class="btn btn-success"  value="ویرایش" />
                </div>
                            <td><a href="<?php echo e(url('admin/users/admins')); ?>" class="btn btn-info">    بازگشت به لیست مدیران</i>  </a></td>


                        </div>
            </div>
            </div>
            </div>
        </form>

    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>