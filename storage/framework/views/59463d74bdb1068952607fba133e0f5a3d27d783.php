
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/pod/add')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <input  name="content_type_id" type="hidden"   value="5">

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>افزودن پادکست</h2></div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <label >عنوان</label>
                            <input  name="title" type="text"  class="form-control" required><br>
                            <label >توضیحات</label>
                            <textarea name="description" type="text"  class="form-control ckeditor" required></textarea>  <br>
                            <label >لینک </label>
                            <input  name="link" type="text"  class="form-control" required><br>
                            <label >تصویر</label>
                            <h6 style="color: red">ارتفاع عکس ها لطفا ثابت باشد(۴۰۰px)</h6>
                            <input  name="image" type="file"  class="form-control" required><br>
                           <hr>
                            <label > عنوان سئو</label>
                            <input  name="title_seo" type="text"  class="form-control"><br>
                            <label >توضیحات سئو</label>
                            <textarea name="description_seo" type="text"  class="form-control "></textarea>  <br>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>