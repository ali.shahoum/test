
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small>سطح دسترسی</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <h3></h3>
                    <form method="POST"  action="<?php echo e(URL::action('Admin\UsersController@postDelete1')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"> جستجو</button>
                        <a href="<?php echo e(url('admin/manage/add_group')); ?>" class="btn btn-success">افزودن     </a>
                        <a href="<?php echo e(url('admin/manage/edit_group')); ?>" class="btn btn-info">ویرایش     </a>

                        <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
                                        </center>
                                        
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">وضعیت</th>
                                    <!--<th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>-->
                                    <th class="column-title" style="display: table-cell;">عملیات</th>


                                </tr>
                                </thead>

                                <tbody>

                                <?php if(count($role) == 0): ?>
                                    <h1> چیزی یافت نشد</h1>
                                <?php else: ?>
                                    <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "><?php echo e($row->name); ?></td>
                                            
                                            <td><a href="<?php echo e(url('admin/manage/delete/'.$row->id)); ?>" class="btn btn-danger"><i class="fa fa-trash"> </i>    </a><a href="<?php echo e(url('admin/manage/edit_group/'.$row->id)); ?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                            

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>


                                </tbody>
                                <center> <?php if(count($role)): ?> <?php echo $role->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                            </table>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">جستجوی پیام </h4>
                </div>
                <form method="get" action="<?php echo e(URL::current()); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="search" value="search">

                    <div class="modal-body">
                        <input type="text" class="form-control"  name="id" placeholder="کد اسلایدر"><br>
                        <input type="text" class="form-control"  name="title" placeholder="موضوع"><br>
                        <input type="text" class="form-control"  name="description" placeholder="توضیحات">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                        <button type="submit" class="btn btn-primary">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>

        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>