<?php $__env->startSection('content'); ?>

    <div class="row profile">
        <div class="container">

            <div class="row profile">
                <?php echo $__env->make('profile.nav_right', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="col-md-9">
                <div class="profile-content" style="direction: rtl;">


                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>پروفایل کاربری</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST"  action="<?php echo e(URL::action('Profile\ProfileController@postInfo')); ?>" enctype="multipart/form-data">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group row">
                                        <label for="username" class="col-4 col-form-label">نام</label>
                                        <div class="col-8">
                                            <input value="<?php echo e($user->id); ?>" name="id" type="hidden"/>
                                            <input value="<?php echo e($user->status); ?>" name="status" type="hidden"/>

                                            <input id="username" name="name" value="<?php echo e($user->name); ?>" class="form-control here" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="lastname" class="col-4 col-form-label">نام خانوادگی</label>
                                        <div class="col-8">
                                            <input id="lastname" name="family" value="<?php echo e($user->family); ?>" class="form-control here" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-4 col-form-label">ادرس</label>
                                        <div class="col-8">
                                            <input id="email" name="address" value="<?php echo e($user->address); ?>" class="form-control here" required="required" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lastname" class="col-4 col-form-label">موبایل</label>
                                        <div class="col-8">
                                            <input id="lastname" name="mobile" value="<?php echo e($user->mobile); ?>" class="form-control here" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-4 col-form-label">ایمیل</label>
                                        <div class="col-8">
                                            <input id="email" name="email" value="<?php echo e($user->email); ?>" class="form-control here" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="newpass" class="col-4 col-form-label">رمز عبور جدید</label>
                                        <div class="col-8">
                                            <input id="newpass" name="password" value="" class="form-control here" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                            <button name="submit" type="submit" class="btn btn-primary">ویرایش</button>
                                        </div>
                                    </div>

                                </form>


                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <br>
    <br>
    </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.profile.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>