<script src="<?php echo e(asset('assets/admin/js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/fastclick.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/nprogress.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/bootstrap-progressbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/icheck.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/moment.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/Chart.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.sparkline.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/gauge.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/skycons.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.pie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.time.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.stack.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.resize.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.orderBars.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.flot.spline.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/admin/js/date.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.vmap.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.vmap.world.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/jquery.vmap.sampledata.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/custom.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/style.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/toastr.js')); ?>"></script>


<!-- bootstrap-daterangepicker -->


<!-- iCheck -->
<!-- Datatables -->
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-buttons/js/buttons.flash.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-buttons/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-buttons/js/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/jszip/dist/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/pdfmake/build/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/vendors/build/vfs_fonts.js')); ?>"></script>

<!-- Custom Theme Scripts -->

<!-- Custom Theme Scripts -->
<?php echo $__env->yieldContent('js'); ?>

