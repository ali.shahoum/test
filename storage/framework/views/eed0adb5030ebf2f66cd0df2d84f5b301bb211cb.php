<?php $__env->startSection('content'); ?>

    <div class="right_col" role="main">
        <div class="panel panel-success">
            <div class="panel-heading"><h2>پروژه های انجام شده</h2></div>

            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                        <a type="button" href="<?php echo e(url('admin/services/add_ads')); ?>" class="btn btn-success">  چاپ و تبلیغات</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="<?php echo e(url('admin/services/edit_ads')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش متن  </h2></div>

                    <div class="panel-body">
                        <div class="col-md-11">
                            <input  name="kind" type="hidden"  class="form-control" value="ads"><br>
                            <input  name="id" type="hidden"  class="form-control" value="<?php echo e(@$ads->id); ?>"><br>
                            <input  name="status" type="hidden"  class="form-control" value="services"><br>
                            <label >موضوع</label>
                            <input  name="title" type="text"  class="form-control" value="<?php echo e(@$ads->title); ?>">
                            <label >توضیحات</label>
                            <textarea  name="description" type="text"  class="form-control ckeditor"><?php echo e(@$ads->description); ?></textarea>
                            <label >تصویر</label><br>
                            <td><img style="width: 100px "  <?php if(@$ads->image): ?>src="<?php echo e(asset('assets/images/services/'.@$ads->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/images/article/article.png')); ?>"<?php endif; ?>><br></td>
                            <input  name="image" type="file" value="<?php echo e(@$ads->image); ?>">
                            <hr>
                            <hr>
                            <label >موضوع سئو</label>
                            <input  name="title_seo" type="text"  class="form-control" value="<?php echo e($ads->title_seo); ?>"><br>

                            <label > توضیحات سئو</label>
                            <textarea  name="description_seo" type="text"  class="form-control "><?php echo e($ads->description_seo); ?></textarea><br>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <br><button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>