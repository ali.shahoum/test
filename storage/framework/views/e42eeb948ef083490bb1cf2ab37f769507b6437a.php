<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <small>اطلاعات شخصی</small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                
                                    
                                    
                                        
                                        
                                        
                                        
                                    
                                
                                
                                
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form class="form-horizontal form-label-left" method="POST" action="<?php echo e(URL::action('Profile\ProfileController@postUserChange')); ?>" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <span class="section"></span>
                                <input  name="id" value="<?php echo e($user->id); ?>" required="required" type="hidden">

                                <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">نام
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" class="form-control col-md-7 col-xs-12"  name="name" value="<?php echo e($user->name); ?>" placeholder="نام" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="family">نام خانوادگی
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="family" class="form-control col-md-7 col-xs-12"  name="family" value="<?php echo e($user->family); ?>" placeholder="نام خانوادگی" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">ایمیل
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="email" class="form-control col-md-7 col-xs-12"  name="email" value="<?php echo e($user->email); ?>" placeholder="ایمیل" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">شماره همراه
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="mobile" class="form-control col-md-7 col-xs-12"  name="mobile" value="<?php echo e($user->mobile); ?>" placeholder="شماره همراه" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">رمز ورود
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="password" class="form-control col-md-7 col-xs-12"  name="password"  placeholder="تغییر رمز عبور"  type="password">
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        
                                        <button id="send" type="submit" class="btn btn-success">ارسال</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>        </div>
    </div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.profile.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>