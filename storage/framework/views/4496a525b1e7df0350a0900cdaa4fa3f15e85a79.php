
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
    <div class="col-md-8 col-sm-8 col-xs-8">

        <div class="x_panel">
            <div class="x_title">
                <h2>داشبورد
                    <small>اسلایدر</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <h3>لیست اسلایدرها</h3>
                <form method="POST" action="<?php echo e(URL::action('Admin\SliderController@postDelete')); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    
                    <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                            data-toggle="tooltip"
                            data-original-title="حذف موارد انتخابی"
                            class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                    </button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"> جستجو</button>


                <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <center>
                                    <input type="checkbox" name="select_all" value="1" id="select_all">
                                </center>
                                
                            </th>
                            <th class="column-title" style="display: table-cell;">شماره</th>
                            <th class="column-title" style="display: table-cell;">موضوع</th>
                            <th class="column-title" style="display: table-cell;">تصویر</th>
                            <th class="column-title" style="display: table-cell;">صفحه اول</th>
                            <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>
                            <th class="column-title" style="display: table-cell;">عملیات</th>


                        </tr>
                        </thead>

                        <tbody>

                        <?php if(count($slider) == 0): ?>
                            <h1> چیزی یافت نشد</h1>
                        <?php else: ?>
                            <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <center>
                                            <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                   type="checkbox"
                                                   value="<?php echo e($row->id); ?>"/>

                                        </center>
                                        
                                    </td>
                                    <td class=" "><?php echo e($key+1); ?></td>
                                    <td class=" "><?php echo e($row->title); ?></td>
                                    <td class=" "> <img style="width: 100px " src="<?php echo e(asset('assets/images/slider/'.$row->image)); ?>"></td>

                                    <td class=" "><?php if($row->first_page): ?> نمایش <?php else: ?> عدم نمایش <?php endif; ?></td>
                                    <td><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></td>
                                    <td><a href="<?php echo e(url('admin/slider/delete/'.$row->id)); ?>" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');" class="btn btn-danger"><i class="fa fa-trash"></i></a><a href="<?php echo e(url('admin/slider/edit/'.$row->id)); ?>" class="btn btn-success">  <i class="fa fa-edit"></i></a></td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>


                        </tbody>
                        <center> <?php if(count($slider)): ?> <?php echo $slider->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                    </table>
                </div>

                </form>
            </div>
        </div>
    </div>
    <div class="col-xs-4">

        <div>
                <div class="alert " style="direction: rtl; margin: 0px auto;">
                    <span style="font-size: 16px;" class="btn btn-info"> با درگ کردن ترتیب مورد نظر را انتخاب نمایید.  </span>
                </div>
                <div id="response"></div>
                <ul id="list-1" class="sortable">
                    <?php $__currentLoopData = $sort; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rowSort): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <li style="direction: rtl; margin: 0px auto;" id="arrayorder_<?php echo stripslashes($rowSort['id']); ?>"><?php echo stripslashes($rowSort['title']); ?>

                        </li>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">جستجوی اسلایدر</h4>
                </div>
                <form method="get" action="<?php echo e(URL::current()); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="search" value="search">

                    <div class="modal-body">
                        <input type="text" class="form-control"  name="title" placeholder=" موضوع"><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                        <button type="submit" class="btn btn-primary">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });
        $(document).ready(function () {

                $("#list-1").sortable({
                    opacity: 0.8, cursor: 'move', update: function () {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        var order = $(this).sortable("serialize") + '&update=update' + '&_token=' + CSRF_TOKEN;
                        $.post("<?php echo URL::action('Admin\SliderController@postSort'); ?> ", order, function (theResponse) {
                            // $("#response").html(theResponse);
                            // $("#response").slideDown('slow');
                            swal("ترتیب با موفقیت تغییر کرد", " ", "success");
                            slideout();
                        });

                    }
                });


        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>