<div class="col-md-3 left_col hidden-print">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            
        </div>

        <div class="clearfix">
        </div>

        <!-- menu profilex quick info -->
        <div class="profile clearfix">
            <div >
                <center><h1>پنل مدیریت</h1></center>

                <img  style="height: 100px" <?php if($user->image): ?>src="<?php echo e(asset('assets/images/users/'.$user->image)); ?>"<?php else: ?> src="<?php echo e(asset('img/users.png')); ?>"<?php endif; ?> alt="..." class="img-circle profile_img">
                <center><h3><?php echo e($user->name); ?> <?php echo e($user->family); ?>  </h3></center>
            </div>
            <!--<div class="profile_info">-->


            <!--</div>-->
        </div>
        <!-- /menu profilex quick info -->

        <!--<br/>-->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                
                <ul class="nav side-menu">
                    <?php if(Auth::user()->hasPermission('slider')): ?>
                        <li><a><i class="fa fa-sliders"></i> اسلایدر <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/slider/list')); ?>">لیست اسلایدر</a></li>
                                <li><a href="<?php echo e(url('admin/slider/add')); ?>">افزودن اسلایدر</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                        <?php if(Auth::user()->hasPermission('slider')): ?>
                        <li><a><i class="fa fa-sliders"></i> اکسل <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/getExport')); ?>">لیست </a></li>
                            </ul>
                        </li>
                    <?php endif; ?>


                    <?php if(Auth::user()->hasPermission('article')): ?>
                        <li><a><i class="fa fa-newspaper-o"></i> مقالات <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/article/list')); ?>">لیست مقالات</a></li>
                                <li><a href="<?php echo e(url('admin/article/add')); ?>">افزودن مقالات</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if(Auth::user()->hasPermission('gallery')): ?>
                        <li><a><i class="fa fa-photo"></i> گالری تصاویر <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/gallery/list')); ?>">لیست </a></li>
                                <li><a href="<?php echo e(url('admin/gallery/add')); ?>">افزودن </a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if(Auth::user()->hasPermission('services')): ?>
                        <li><a><i class="fa fa-wrench"></i> خدمات <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/services/loges')); ?>">غرفه ها</a></li>
                                <li><a href="<?php echo e(url('admin/services/decor')); ?>">دکوراسیون سبز داخلی</a></li>
                                <li><a><i class="fa fa-bars"></i>سازه های دکوراسیون
                                        <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?php echo e(url('admin/services/decor/roof')); ?>"> بام سبز</a></li>
                                        <li><a href="<?php echo e(url('admin/services/decor/pasio')); ?>">پاسییو سبز</a></li>
                                        <li><a href="<?php echo e(url('admin/services/decor/wall')); ?>">دیوار سبز</a></li>
                                        <li><a href="<?php echo e(url('admin/services/decor/balkon')); ?>">بالکن سبز</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo e(url('admin/services/ads')); ?>">چاپ و تبلیغات</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                        <?php if(Auth::user()->hasPermission('ticket')): ?>
                            <li><a><i class="fa fa-ticket"></i> تیکت <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo e(url('admin/ticket/list')); ?>">لیست تیکت</a></li>
                                </ul>
                            </li>
                        <?php endif; ?>

                    

                    
                    
                    
                    
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    

                    
                    
                    
                    <?php if(Auth::user()->hasPermission('pod')): ?>
                        <li><a><i class="fa fa-film"></i>پادکست ها<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/pod/list')); ?>">لیست </a></li>
                                <li><a href="<?php echo e(url('admin/pod/add')); ?>">افزودن </a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if(Auth::user()->hasPermission('setting')): ?>
                        <li><a><i class="fa fa-cogs"></i> تنظیمات <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/setting/edit')); ?>">نمایش </a></li>
                                


                            </ul>
                        </li>
                    <?php endif; ?>
                    
                    
                    
                    
                    

                    
                    
                    
                    <?php if(Auth::user()->hasPermission('users')): ?>
                        <li><a><i class="fa fa-users"></i>لیست کاربران<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                
                                <li><a href="<?php echo e(url('admin/users/admins')); ?>">مدیران</a></li>

                            </ul>
                        </li>
                    <?php endif; ?>

                    
                    
                    
                    
                    

                    
                    
                    
                    <?php if(Auth::user()->hasPermission('manage')): ?>
                        <li><a><i class="fa fa-eye-slash"></i>سطح دسترسی<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/manage/manage')); ?>">جدول</a></li>

                            </ul>
                        </li>
                    <?php endif; ?>
                    
                        <li><a><i class="fa fa-sitemap"></i> سایت مپ <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/siteMap')); ?>">سایت مپ </a></li>
                            </ul>
                        </li>
                    
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    
                    
                    

                    
                    
                    
                    <?php if(Auth::user()->hasPermission('uploader')): ?>
                        <li><a><i class="fa fa-cloud-upload"></i>اپلودر<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/uploader/list')); ?>">لیست </a></li>

                            </ul>
                        </li>
                    <?php endif; ?>
                    <br>
                    <br>
                    <br>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" onclick="return confirm('آیا برای خروج مطمئن هستید.');" data-placement="top" title="خروج" href="<?php echo e(url('admin/logout')); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
