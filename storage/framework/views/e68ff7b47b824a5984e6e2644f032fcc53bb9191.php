<!DOCTYPE html>
<html>

<head>
    <title>جشنواره انتخاب نام برند</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link href="<?php echo e(asset('assets/landing/css/style.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/login.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/form.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/sm-ui.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/animate.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('assets/landing/js/jquery.min.js')); ?>"></script>


    <script src="<?php echo e(asset('assets/landing/js/jquery-3.3.1.min.js')); ?>"></script>
    
    
    <script src="<?php echo e(asset('assets/landing/js/dropdown.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/landing/js/bootstrap.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/landing/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/landing/js/sm-ui.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/landing/js/form.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/landing/js/home.js')); ?>"></script>

    <!--<link rel="stylesheet" type="text/css" href="css/dropdown.css">-->
    <!---->
    <link href="<?php echo e(asset('assets/landing/css/font-awesome.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/bootstrap.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/landing/css/bootstrap.min.css')); ?>" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo e(asset('css/toastr.css')); ?>">


</head>

<body>
<div class="container">
    <div class="row top-m">
        <div class="col-md-6" style="visibility: visible; animation-duration: 3s; animation-name: fadeInLeft;">
            <img class="img-responsive" src="<?php echo e(asset('assets/landing/img/1.png')); ?>">
        </div>
        <div class="col-md-6">
            <div class="title-one"
                 style="visibility: visible; animation-duration: 3s; animation-name: fadeInRight;">
                <i class="icon-1"></i>

                <h1>

                    به جشنواره انتخاب نام برند خوش آمدید
                    <!-- به جشنواره انتخاب نام برند <span class="underline">محبوب</span> خود خوش آمدید -->
                </h1>
                <hr class="underline"></hr>
                <p>
                    از همکاری شما سپاسگزاریم
                </p>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="title-one" style="visibility: visible; animation-duration: 3s; animation-name: fadeInLeft;">
                <h1>
                    با هویت برند آشنا شوید تا بهتر و راحتتر در این جشنواره شرکت کنید
                    <!-- <span class="underline">هویت</span> برند را بشناسید تا بهتر و راحت تر در این جشنواره شرکت کنید -->
                </h1>
                <hr class="underline"></hr>
                <!-- <p>
                    هزینه و زمان زیادی برای ساخت لندینگ پیج ( صفحه فرود ) توسط تیم محصول صرف می کنید. و در آخر هم
                    ممکن است، صفحه فرود شما بدلیل استاندارد نبودن، نرخ تبدیل مناسبی نداشته و مشتریان خود را از دست
                    می‌دید. متناسب با نیاز کاربران صفحه فرود اختصاصی بسازید. لندین این بستر رو برای شما فراهم کرده
                    است.
                </p> -->
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <i class="icon-2" style="visibility: visible; animation-duration: 3s; animation-name: fadeInRight;">
                <div class="h_iframe-aparat_embed_frame"><span
                            style="display: block;padding-top: 57%"></span><iframe
                            src="https://www.aparat.com/video/video/embed/videohash/GSelL/vt/frame"
                            allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
                </div>
            </i>
        </div>
    </div>
    <div class="row back-holder dir-rtl">
        <span class="back-img hidden-xs hidden-sm"></span>
        <div class="col-md-12 text-center title-one mrg20T"
             style="visibility: visible; animation-duration: 10s; animation-name: fadeInDown;">
            <h1 class="text-center">
                مراحل شرکت در مسابقه
            </h1>
            <hr class="underline"></hr>
        </div>
        <div class="col-md-3">
            <div class="text-center mosabeghe">
                <i class="icon-3 "></i>
                <h3>
                    خلق نام
                </h3>
                <p>
                    نام پیشنهادی شما
                </p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="text-center mosabeghe">
                <i class="icon-4"></i>
                <h3>
                    تکمیل فرم و ارسال نام پیشنهادی
                </h3>
                <p>
                    تکمیل فرم جهت تسهیل ارتباط ما با شما در صورت برنده شدن
                </p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="text-center mosabeghe">
                <i class="icon-5"></i>
                <h3>
                    تحلیل و بررسی پیشنهادهای ارسالی
                </h3>
                <p>
                    بررسی اسامی پیشنهادی طبق هویت برند جدید

                </p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="text-center mosabeghe">
                <i class="icon-6"></i>
                <h3>
                    دریافت جوایزجشنواره
                </h3>
                <p>
                    جایزه نقدی به مبلغ ٢ میلیون تومان به نام برگزیده
                    <br />
                    ارسال دعوتنامه و آفر ویژه افتتاحیه تنها به ده نفراز شرکت کنندگان به قید قرعه
                    <br />
                    اعلام نتایج ١۵ الی ٢٠ روز آینده ازپیج رادیو قرمز

                </p>
            </div>
        </div>
    </div>

    <div class="row ui form-rtl form">
        <div class="start col-md-6 title-one mrg20T  pad0R ">
            <div class="row pad0R">
                <div class="col-md-3 col-lg-4 goldan pad0R pad0L">
                    <span class="icon-7"></span>
                </div>
                <div class="col-md-10 col-lg-8 pad0R">
                    <h1>
                        <span class="back-color">/</span>
                        همین حالا شروع کنید

                    </h1>
                    <p>
                        بهترین اسم را پیشنهاد دهید تا برنده جایزه ویژه ما باشید
                    </p>
                </div>

            </div>
        </div>
        <div class="col-md-6 pad0R">
                <span class="icon-10">
                     <form id="loginform" method="POST" id="loginform" action="<?php echo e(URL::action('Site\SiteController@postLanding')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                         <div class="dir-ltr mrg15B">
                         <div class="ui  icon input field placeholder">
                         <input data-val="true" data-val-regex="نام و نام خانوادگی"
                         data-val-required="نام و نام خانوادگی" placeholder="نام و نام خانوادگی" id="Name"
                         name="name" type="text" required oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را وارد کنید')"
                         oninput="setCustomValidity('')">
                         <!-- <label for="Name" class="active">
                         نام و نام خانوادگی
                         </label> -->
                         <i class="icon-11 icon large" aria-hidden="true"></i>
                         </div>
                         </div>
                         <div class="dir-ltr mrg15B">
                         <div class="ui  icon input field placeholder login-field">
                         <input data-val="true" data-val-regex="شماره تماس" data-val-required="شماره تماس" minlength="10" maxlength="11"
                         id="Number" name="phone" placeholder="شماره تماس" type="text" required oninvalid="this.setCustomValidity('لطفاشماره تماس را وارد کنید')"
                         oninput="setCustomValidity('')">
                         <!-- <label for="Number" class="active">
                         شماره تماس
                         </label> -->
                         <i class="icon-12 icon large"></i>

                         </div>
                         </div>
                         <div class="dir-ltr mrg15B">
                         <div class="ui  icon input field placeholder login-field">
                         <input data-val="true" data-val-regex="آیدی اینستاگرام"
                         data-val-required="آیدی اینستاگرام" id="Instagram" placeholder="آیدی اینستاگرام"
                         name="insta" type="text" required oninvalid="this.setCustomValidity('لطفا آیدی اینستاگرام را وارد کنید')"
                         oninput="setCustomValidity('')">
                         <!-- <label for="Instagram" class="active">
                         آیدی اینستاگرام
                         </label> -->
                         <i class="icon-13 icon large"></i>

                         </div>
                         </div>
                         <div class="dir-ltr mrg15B">
                         <div class="ui  icon input field placeholder login-field">
                         <input data-val="true" data-val-regex=" نام پیشنهادی" data-val-required="نام پیشنهادی"
                         id="NameP" name="suggest" placeholder="نام پیشنهادی " type="text" required  oninvalid="this.setCustomValidity('لطفا نام پیشنهادی را وارد کنید')"
                         oninput="setCustomValidity('')">
                         <!-- <label for="NameP" class="active">
                         نام پیشنهادی
                         </label> -->
                         <i class="icon-14 icon large"></i>

                         </div>
                         </div>
                         <div class="text-left btn-box">
                         <button id="SubmitButton" name="btnSubmit" type="submit" value="Submit"
                         onclick="SaveMessage()" class="ui primary left labeled icon button submit">
                         ارسال
                         <i class="pencil alternate icon medium-icon"></i>
                         </button>
                         </div>
                    </form>
                </span>
        </div>
    </div>
    <div class="row ">
        <div class="title-one mrg20T  col-md-12 ">
            <h1 class="text-center">
                سوالات متداول
            </h1>
            <hr class="underline"></hr>
        </div>
        <div class="trigger example col-md-12">
            <div id="integration-list">
                <ul>
                    <li>
                        <a class="expand">
                            <div class="right-arrow">+</div>
                            <div>
                                <h2>
                                    نام منتخب را چگونه انتخاب می کنید ؟
                                </h2>
                            </div>
                        </a>
                        <div class="detail">
                            <div id="right">
                                <div id="sup">
                                    <div>
                                        <p>
                                            اسامی ارسالی توسط تیم مارکتینگ و برندینگ طبق شاخه های تعریف شده برای
                                            هویت برند جدید طلا آنالیز و تحلیل می شود و در نهایت یک اسم که دارای
                                            بالاترین تناسب را داشت برگزیده می شود . توضیح بیشتر در ویدیو بالا ببینید

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="expand">
                            <div class="right-arrow">+</div>
                            <div>
                                <h2>
                                    اگر نام منتخب توسط چند نفر پیشنهاد داده شده بود نفر منتخب چگونه انتخاب میشود؟
                                </h2>
                            </div>
                        </a>
                        <div class="detail">
                            <div id="right">
                                <div id="sup">
                                    <div>
                                        <p>
                                            در صورت برگزیده شدن نام مشابه بین افراد شرکت کننده قرعه کشی می شود
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="expand">
                            <div class="right-arrow">+</div>
                            <div>
                                <h2>
                                    چه روزی و چگونه نتیجه جشنواره اعلام میشود؟
                                </h2>
                            </div>
                        </a>
                        <div class="detail">
                            <div id="right">
                                <div id="sup">
                                    <div>
                                        <p>
                                            از ١۵ الی ٢٠ روز آینده از طریق پیج رادیو قرمز
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="expand">
                            <div class="right-arrow">+</div>
                            <div>
                                <h2>
                                    برای شرکت کنندگانی که منتخب نشده اند برنامه ای دارید؟
                                </h2>
                            </div>
                        </a>
                        <div class="detail">
                            <div id="right">
                                <div id="sup">
                                    <div>
                                        <p>
                                            بله. به قید قرعه برای ده نفر از شرکت کننده دعوتنامه به همراه تخفیف ویژه
                                            افتتاحیه ارسال خواهد شد
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <i class="icon-9"></i>
    </div>
    <!-- <footer class="footer">
        <div class="row ">
            <div class="col-md-6">
                <h3>
                    <i class="circle color-orange icon"></i>
                    درباره ما
                </h3>
                <p>
                    به عنوان یکی از بزرگترین مراکز آموزش زبان‌های خارجی با بیش از ۳۰ سال تجربه و بیش از ۱۳۰ شعبه
                    فعال در سراسر کشور مفتخریم تا بهترین کیفیت آموزشی را با کمترین هزینه خدمت شما عزیزان ارائه
                    نماییم.
                </p>
            </div>
            <div class="col-md-4">
                <h3>
                    <i class="circle color-orange icon"></i>
                    راههای ارتباطی با ما
                </h3>
                <div class="addres">
                    <i class="icon-18 mrg10L"></i>
                    <span class="mrg10B">
                        تهران، خ انقلاب، مابین خیابان حافظ و ولیعصر، کوچه بالاور، پلاک ۷، طبقه دوم
                    </span>
                    <br />
                    <i class="icon-17 mrg10L"></i>
                    <span class="mrg10B">
                        شماره تماس پشتیبانی: ٠٩١٢۶١۵١٣٧٢
                    </span>
                    <br />
                    <i class="icon-16 mrg10L"></i>
                    <span class="mrg10B">
                        <a href="mailto:pv@kish-ist.net">
                            pv@kish-ist.net
                        </a>
                    </span>
                </div>
            </div>
            <div class="col-md-2 dir-ltr text-left">
                <a href="#" class="mrg10L">
                    <i class="base-social icon-19 mrg20T"></i>
                </a>

                <a href="#" class="mrg10L">
                    <i class="base-social icon-20 mrg20T"></i>
                </a>

                <a href="#" class="mrg10L">
                    <i class="base-social icon-21 mrg20T"></i>
                </a>

                <a href="#" class="mrg10L">
                    <i class="base-social icon-22 mrg20T"></i>
                </a>

                <a href="#" class="mrg10L">
                    <i class="base-social icon-23 mrg20T"></i>
                </a>

            </div>
        </div>
        <hr class="border-dashed">
        <div class="text-center">
            خلق شده با <i class="heart icon color-orange"></i> در لندین
        </div>
    </footer> -->
</div>
</div>

<script src="<?php echo e(asset('assets/landing/js/responsiveslides.min.js')); ?>"></script>

<script src="<?php echo e(asset('js/toastr.js')); ?>"></script>

<script>

    $(function () {


    });
</script>
<?php if(isset($errors)): ?>
    <?php if($errors->any() || Session::has('error')): ?>
        <?php if(Session::has('error')): ?>
            <script>
                var msg = " <?php echo Session::get('error'); ?>";
                Command: toastr["error"](msg, "خطا")
            </script>
        <?php endif; ?>
        <?php if(isset($errors)): ?>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <script>
                    var msg = "<?php echo $error; ?>";
                    Command: toastr["error"](msg, "خطا")
                </script>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<?php if(Session::has('success')): ?>
    <script>
        var msg = "<?php echo Session::get('success'); ?>";
        Command: toastr["success"](msg, "موفق")
    </script>
<?php endif; ?>

<?php if(Session::has('info')): ?>
    <script>
        var msg = "<?php echo Session::get('info'); ?>";
        Command: toastr["info"](msg)
        <?php Session::forget('info'); ?>
    </script>
<?php endif; ?>


</body>

</html>