<!DOCTYPE html>
<html lang="fa" dir="rtl">
<?php echo $__env->make('layout.profile.blocks.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="nav-md">

<div class="container body">

    <div class="main_container">
    <?php echo $__env->make('layout.profile.blocks.nav_right', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layout.profile.blocks.nav_top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



    <?php echo $__env->yieldContent('content'); ?>
    <!-- /page content -->

        <!-- footer content -->

        <?php echo $__env->make('layout.profile.blocks.nav_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->
<?php echo $__env->make('layout.profile.blocks.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layout.site.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


</body>
</html>
