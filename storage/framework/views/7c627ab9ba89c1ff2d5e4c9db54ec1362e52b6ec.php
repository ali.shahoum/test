
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
    <div class="col-md-8 col-sm-8 col-xs-8">

        <div class="x_panel">
            <div class="x_title">
                <h2>داشبورد
                    <small>گالری تصاویر</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <h3>لیست تصاویر گالری</h3>
                <form method="POST" action="<?php echo e(URL::action('Admin\GalleryController@postDelete')); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    
                    <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                            data-toggle="tooltip"
                            data-original-title="حذف موارد انتخابی"
                            class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                    </button>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"> جستجو</button>


                <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th>
                                <center>
                                    <input type="checkbox" name="select_all" value="1" id="select_all">
                                </center>
                                
                            </th>
                            <th class="column-title" style="display: table-cell;">شماره</th>
                            <th class="column-title" style="display: table-cell;">نام</th>
                            <th class="column-title" style="display: table-cell;">تصویر</th>

                            <th class="column-title" style="display: table-cell;">عملیات</th>


                        </tr>
                        </thead>

                        <tbody>

                        <?php if(count($gallery) == 0): ?>
                            <h1> چیزی یافت نشد</h1>
                        <?php else: ?>
                            <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <center>
                                            <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                   type="checkbox"
                                                   value="<?php echo e($row->id); ?>"/>

                                        </center>
                                        
                                    </td>
                                    <td class=" "><?php echo e($key+1); ?></td>
                                    <td class=" "><?php echo e($row->name); ?></td>
                                    <td class=" "> <img style="width: 100px " src="<?php echo e(asset('assets/images/gallery/'.$row->image)); ?>"></td>

                                    <td><a href="<?php echo e(url('admin/gallery/delete/'.$row->id)); ?>" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');" class="btn btn-danger"><i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>


                        </tbody>
                        <center> <?php if(count($gallery)): ?> <?php echo $gallery->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                    </table>
                </div>

                </form>
            </div>
        </div>
    </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">جستجوی تصویر</h4>
                </div>
                <form method="get" action="<?php echo e(URL::current()); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="search" value="search">

                    <div class="modal-body">
                        <input type="text" class="form-control"  name="name" placeholder=" نام"><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                        <button type="submit" class="btn btn-primary">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>