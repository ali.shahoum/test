<?php $__env->startSection('title'); ?>
    <?php if($ads->title_seo==null): ?>
        <?php echo e($ads->title); ?>

    <?php else: ?>
        <?php echo e($ads->title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php if($ads->description_seo == null): ?>
        <?php echo e($ads->description); ?>

    <?php else: ?>
        <?php echo e($ads->description_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(asset('assets/images/services/ads.png')); ?>);margin-top: 60px;">
            <h2></h2>
        </div>

        <div class="container">

        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### About Area Start ##### -->
    <section class="about-us-area">
        <div class="container"><br>
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e('#'); ?>"><i class=""></i> خدمات</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> چاپ و تبلیغات </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-12 col-lg-5">


                </div>


            </div>
        </div>

        <div class="container" style="direction:rtl">
            <div class="row">
                <div class="col-12">
                    <center> <h2><?php echo e(@$ads->title); ?></h2>   </center>
                    <p style="text-align: right"><?php echo $ads->description; ?></p>


                </div>
            </div>
        </div>
    </section>

    <div class=" container col-md-9 " style="  border: 1px solid green;margin-bottom: 10%">
        <!-- Section Heading -->
        <div class="section-heading" style="margin-top: 2%">
            <center><h5>
                    فرم درخواست چاپ و تبلیغات

                </h5></center>
            <p></p>
        </div>
        <!-- Contact Form Area -->
        <div class="contact-form-area mb-100" >
            <form method="POST" action="<?php echo e(URL::action('Site\SiteController@requestDecor')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="row" >

                    <div class="col-12 " >
                        <div class="form-group" >
                            <input type="text" name="family" class="form-control" id="contact-name" style="text-align: right!important;height:55px!important"placeholder="نام و نام خانوادگی" required
                                   oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را وارد کنید')"
                                   oninput="setCustomValidity('')"
                            >

                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="form-group">
                            <input type="tel" name="mobile" minlength="10" maxlength="11" class="form-control" style="text-align: right!important;height:55px!important" id="contact-subject" placeholder="تلفن همراه" required oninvalid="this.setCustomValidity('لطفا شماره همراه خود را وارد کنید')"
                                   oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="col-12">

                        <div class="form-group" style="direction: rtl;">
                            <input type="email" name="email" style="text-align: right!important;height:55px!important" class="form-control" id="contact-email" placeholder="ایمیل" required oninvalid="this.setCustomValidity('لطفا ایمیل خود را وارد کنید')"
                                   oninput="setCustomValidity('')">

                        </div>
                    </div>
                    <div class="col-12">
                        <input type="hidden" name="subject" style="text-align: right!important;height:55px!important" class="form-control"   value="5">

                        
                        
                        
                        
                        
                        
                        
                        

                        
                        
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn alazea-btn mt-15" style="background-color: #577e3b" >ارسال</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>