<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small>مقالات</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <h3>لیست مقالات</h3>
                    <form method="POST" action="<?php echo e(URL::action('Admin\ArticleController@postDelete')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">  جستجو و دسته بندی وضعیت مقالات</button>


                        <div class="table-responsive">


                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input type="checkbox" name="select_all" value="1" id="select_all">
                                        </center>
                                        
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">موضوع</th>
                                    <th class="column-title" style="display: table-cell;">تصویر</th>
                                    <!--<th class="column-title" style="display: table-cell;">وضعیت</th>-->
                                    <th class="column-title" style="display: table-cell;">وضعیت</th>
                                    <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>
                                    <th class="column-title" style="display: table-cell;">تاریخ اخرین ویرایش</th>
                                    <th class="column-title" style="display: table-cell;">عملیات</th>


                                </tr>
                                </thead>

                                <tbody>

                                <?php if(count($article) == 0): ?>
                                    <h1> چیزی یافت نشد</h1>
                                <?php else: ?>
                                    <?php $__currentLoopData = $article; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "><?php echo e($row->title); ?></td>
                                            <td class=" "> <img style="width: 100px " <?php if($row->image): ?>src="<?php echo e(asset('assets/images/article/'.$row->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/images/article/article.png')); ?>"<?php endif; ?>></td>

                                        <!--<td class=" "><?php if($row->status): ?> فعال <?php else: ?> غیرفعال <?php endif; ?></td>-->
                                            <td class=" "><?php if($row->status ===0): ?> <a class="btn btn-default">در حال ویرایش</a> <?php elseif($row->status ===1): ?> <a href="<?php echo e(url('admin/article/share/'.$row->id)); ?>" onclick="return confirm('آیا از انتشاراین مقاله اطلاعات مطمئن هستید.');" class="btn btn-warning">در صف انتشار</a> <?php elseif($row->status ===2): ?> <a class="btn btn-dark">منتشر شده</a><?php endif; ?></td>
                                            <td><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></td>
                                            <td><?php echo e(jdate('Y/m/d H:i',@$row->updated_at->timestamp)); ?></td>
                                            <td><a href="<?php echo e(url('admin/article/delete/'.$row->id)); ?>" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');" class="btn btn-danger"><i class="fa fa-trash"></i></a><a href="<?php echo e(url('admin/article/edit/'.$row->id)); ?>" class="btn btn-success">  <i class="fa fa-edit"></i></a></td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>


                                </tbody>
                                <center> <?php if(count($article)): ?> <?php echo $article->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                            </table>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">جستجوی مقاله</h4>
                </div>
                <form method="get" action="<?php echo e(URL::current()); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="search" value="search">

                    <div class="modal-body">
                        <p style="color:red;">جستجوی مقاله از طریق نام </p>
                        <input type="text" class="form-control"  name="title" placeholder="موضوع"><br>
                        <hr>
                        <p style="color:red;">نمایش دسته بندی مقالات</p>

                        <div class="col-md-12">
                            <div class="row">
                                <select  id="selectbasic" name="status" class="form-control">
                                    <option value="">  نمایش کل مقالات</option>
                                    <option value="0">  نمایش لیست در حال ویرایش</option>
                                    <option value="1">نمایش لیست در صف  انتشار</option>
                                    <option value="2">نمایش لیست منتشر شده </option>

                                </select>
                            </div>
                        </div>
                    </div><hr>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                        <button type="submit" class="btn btn-primary">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>