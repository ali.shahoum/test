<?php $__env->startSection('content'); ?>
    <?php echo $__env->make("layout.site.blocks.product", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make("layout.site.blocks.services", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make("layout.site.blocks.decor", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make("layout.site.blocks.article", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make("layout.site.blocks.podCast", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make("layout.site.blocks.coowork", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>