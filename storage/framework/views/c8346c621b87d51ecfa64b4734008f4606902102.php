<header class="header-area" >


<!-- ***** Navbar Area ***** -->
    <div class="alazea-main-menu" >
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="alazeaNav">

                    <!-- Nav Brand -->
                    <a href="<?php echo e(url('/')); ?>" class="nav-brand">
                        
                        <p style="padding-top: 20%">پادییو</p>
                    </a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu" >

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Navbar Start -->
                        <div class="classynav" style="direction: rtl;text-align: right!important;">
                            <ul>
                                <li><a href="<?php echo e(url('/')); ?>">خانه</a></li>
                                <li><a href="#">خدمات پادییو</a>
                                    <ul class="dropdown">
                                        <li><a href="<?php echo e(url('/loges')); ?>">غرفه های نمایشگاهی</a></li>
                                        <li><a href="<?php echo e(url('/decor')); ?>">دکوراسیون سبز داخلی</a>
                                            <ul class="dropdown">
                                                <li><a href="<?php echo e(url('/wall')); ?>">دیوار سبز</a></li>
                                                <li><a href="<?php echo e(url('/roof')); ?>">بام سبز</a></li>
                                                <li><a href="<?php echo e(url('/pasio')); ?>">پاسییو سبز</a></li>
                                                <li><a href="<?php echo e(url('/balkon')); ?>">بالکن سبز</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="<?php echo e(url('/ads')); ?>">چاپ و تبلیغات</a></li>

                                    </ul>
                                </li>
                                <li><a href="#">کاتالوگ پادییو</a>
                                    <ul class="dropdown">
                                        
                                            
                                        
                                    </ul>
                                </li>
                                <li><a href="#">مجله پادییو</a>
                                    <ul class="dropdown">
                                        <li><a href="<?php echo e(url('/article')); ?>">مقالات</a></li>
                                        <li><a href="<?php echo e(url('/pod')); ?>">پادکست ها</a></li>
                                    </ul>
                                </li>

                                <li><a href="<?php echo e(url('/about')); ?>">درباره ما</a></li>

                                <li><a href="<?php echo e(url('/contact')); ?>">تماس با ما</a></li>
                                <li><a onclick="document.getElementById('id01').style.display='block'" class="">عضویت/ورود</a></li>


                                
                            </ul>

                            <!-- Search Icon -->
                            
                            
                            

                        </div>
                        <!-- Navbar End -->
                    </div>
                </nav>

                <!-- Search Form -->
                <div class="search-form">
                    <form action="#" method="get">
                        <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
                        <button type="submit" class="d-none"></button>
                    </form>
                    <!-- Close Icon -->
                    <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div id="id01" class="w3-modal" style="direction: rtl!important;">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom">
            <header class="w3-container w3-blue">
   
         
                <h4>ورود به پادییو</h4>
            </header>

            <div class="w3-bar w3-border-bottom">
                <button class="tablink w3-bar-item w3-button" onclick="openCity(event, 'Login')">ورود</button>
                <button class="tablink w3-bar-item w3-button" onclick="openCity(event, 'Register')">عضویت</button>

            </div>

            <div id="Login" class="w3-container city">
                <div class="w3-center"><br>
                    
                    
                </div>

                <form class="w3-container" method="POST" action="<?php echo e(URL::action('Auth\LoginController@postLogInUser')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="w3-section">
                        
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="شماره همراه" name="mobile" required>
                        
                        <input class="w3-input w3-border" type="password" placeholder="رمز عبور" name="password" required>
                        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">ورود</button>
                        
                    </div>
                </form>

                
                    
                    
                


            </div>

            <div id="Register" class="w3-container city">
                <form class="w3-container" method="POST" action="<?php echo e(URL::action('Site\SiteController@postRegister')); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="w3-section">
                        
                        <input class="w3-input w3-border w3-margin-bottom" type="hidden"  name="admin" value="0" required>
                        <input class="w3-input w3-border w3-margin-bottom" type="hidden"  name="status" value="0" required>
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="نام" name="name" required>
                        
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="نام خانوادگی" name="family" required>
                        
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="ایمیل" name="email" required>
                        
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="شماره همراه" name="mobile" required>
                        
                        <input class="w3-input w3-border" type="password" placeholder="رمز عبور" name="password" required>
                        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">ثبت نام</button>
                        
                    </div>
                </form>
            </div>


            <div class="w3-container w3-light-grey w3-padding">
                <button class="w3-button w3-left w3-white w3-border"
                        onclick="document.getElementById('id01').style.display='none'">خروج</button>
            </div>
        </div>
    </div>
</header>

    <script>
    document.getElementsByClassName("tablink")[0].click();

    function openCity(evt, cityName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].classList.remove("w3-light-grey");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.classList.add("w3-light-grey");
    }
</script>

