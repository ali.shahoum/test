<div class="team_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-95 team_area_section_title" style="">
                    <span class="sub_heading"></span>
                    <h4>خدمات پادییو</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-xl-4 col-md-4">
                <div class="single_team text-center">
                    <a href="<?php echo e(url('/'.$row->kind)); ?>">
                    <div class="thumb team_1" style="background-image: url(<?php echo e(url('assets/images/services/'.$row->image)); ?>);">
                        <div class="author_links">
                            
                                
                                
                                
                            
                        </div>
                    </div>
                    </a><br>
                    <a href="<?php echo e(url('/'.$row->kind)); ?>" style="color: #577e3b!important;"><?php echo e($row->title); ?></a>
                    <p></p>
                </div>
            </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
</div>