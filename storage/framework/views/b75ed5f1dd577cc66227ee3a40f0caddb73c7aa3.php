<?php $__env->startSection('title'); ?>
    <?php if($setting->article_title_seo==null): ?>
        <?php echo e($setting->title); ?>

    <?php else: ?>
        <?php echo e($setting->article_title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php echo e(@$setting->article_description_seo); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/images/article/article_banner.png')); ?>);margin-top: 60px;">
            <h2></h2>
        </div>



    </div>

    <section class="alazea-blog-area section-padding-100-0">
        <div class="container">
            
                
                    
                    
                        
                        
                    
                
            
                <div class="row" style="direction: rtl">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e('/article'); ?>">   مقالات  </a></li>
                            </ol>
                        </nav>
                    </div>
            </div>

            <div class="row justify-content-center" style="text-align: center">
                <?php if(count($article) == 0): ?>

                        <div class="">
                            <br>
                            <br>  <br>
                            <br>
                            <br>
                            <br>
                            <h5> متاسفانه مقاله ای با نام جستجو شده یافت نشد</h5>
                            <br>
                            <br>
                            <br>  <br>
                            <br>
                            <br>  <br>
                            <br>
                            <br>
                        </div>


                <?php else: ?>
                <!-- Single Blog Post Area -->
                <?php $__currentLoopData = $article; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-blog-post mb-100">
                            <div class="post-thumbnail mb-30">

                                <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>"><img src="<?php echo e(asset('assets/images/article/'.$row->image)); ?>" alt="" style="width: 350px;height: 350px"></a>
                            </div>
                            <div class="post-content">
                                <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>" class="post-title"><center>
                                        <h5>
                                            <?php echo e($row->title); ?>   </h5>
                                    </center></a>
                                <div class="post-meta">
                                    
                                    
                                </div>
                                <p class="post-excerpt">
                                    <?php echo \Illuminate\Support\Str::words($row->description_first, 15,'....'); ?>

                                    <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>" class=''>بیشتر</a>         </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                

                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                

            </div>
        </div>
    </section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>