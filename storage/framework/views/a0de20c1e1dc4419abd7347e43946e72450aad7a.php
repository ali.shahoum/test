
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <div class="col-md-10 col-sm-10 col-xs-10">
            <div class="x_panel">
                <div class="x_title">
                    <h2>لیست محصولات
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers" >
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border-bottom: 4px solid #e6e9ed;">
                                    <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <a href="<?php echo e(url('admin/show/showTime/'.$row->id)); ?>"  class="btn btn-success"><?php echo e($row->name); ?></a>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div><br>
                            </div>
                    </div>
                    <br>
                    <?php if(@$pro['price1'] or @$pro['price2']or@$pro['price3']or@$pro['price4']): ?>
                        <h5 >لیست زمانهای تعین شده برای محصول<?php echo e($pro->name); ?>

                        </h5>
                        <div class="clearfix"></div>

                        <div class="bs-example-popovers">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <?php if(@$pro['price1']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-dark" style="margin-right: 30px">صبح</a><?php endif; ?>
                                    <?php if(@$pro['price2']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-dark" style="margin-right: 30px">ظهر</a><?php endif; ?>
                                    <?php if(@$pro['price3']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-dark" style="margin-right: 30px">عصر</a><?php endif; ?>
                                    <?php if(@$pro['price4']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-dark" style="margin-right: 30px">شب</a><?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(@$pro['price1'] or @$pro['price2']or@$pro['price3']or@$pro['price4']): ?>
                        <div class="clearfix"></div>

                        <div class="bs-example-popovers">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <?php if(@$pro['price1']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-default" style="margin-right: 30px"><?php echo e($pro['price1']); ?></a><?php endif; ?>
                                    <?php if(@$pro['price2']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-default" style="margin-right: 30px"><?php echo e($pro['price2']); ?></a><?php endif; ?>
                                    <?php if(@$pro['price3']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-default" style="margin-right: 30px"><?php echo e($pro['price3']); ?></a><?php endif; ?>
                                    <?php if(@$pro['price4']): ?><a href="<?php echo e(url('admin/show/showPrice/'.$pro->id)); ?>" class="btn btn-default" style="margin-right: 30px"><?php echo e($pro['price4']); ?></a><?php endif; ?>
                                </div>
                            </div>
                        </div>
                <?php endif; ?>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>


    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>