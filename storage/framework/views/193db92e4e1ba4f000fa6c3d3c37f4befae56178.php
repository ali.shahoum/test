<section class="alazea-blog-area section-padding-100-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h4>اخرین مقالات</h4>
                    <p></p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" style="text-align: center">

            <!-- Single Blog Post Area -->
            <?php $__currentLoopData = $article; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            


                <div class="col-12 col-md-4 col-lg-4 col-sm-4 ">
                    <div class="single-blog-post mb-100">
                        <div class="post-thumbnail mb-30">
                            
                            <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>"><img src="<?php echo e(asset('assets/images/article/'.$row->image)); ?>" alt="" style="width: 350px;height: 350px"></a>
                        </div>
                        <div class="post-content">
                            <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>" class="post-title"><center>
                                    <h5>
                                        <?php echo e($row->title); ?>   </h5>
                                </center></a>
                            <div class="post-meta">
                                
                                
                            </div>
                            <p class="post-excerpt">
                                <?php echo \Illuminate\Support\Str::words($row->description_first, 10,'....'); ?>

                                <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>" class=''>بیشتر</a>         </p>
                        </div>
                    </div>
                </div>

            
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


    </div>
    </div>
</section>
