<div class="col-md-3 left_col hidden-print">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            
        </div>

        <div class="clearfix">
        </div>

        <!-- menu profilex quick info -->
        <div class="profile clearfix">
            <div >
                <center><h1>پنل </h1></center>

                <img  style="height: 100px" <?php if($user->image): ?>src="<?php echo e(asset('assets/images/users/'.$user->image)); ?>"<?php else: ?> src="<?php echo e(asset('img/users.png')); ?>"<?php endif; ?> alt="..." class="img-circle profile_img">
                <center><h3><?php echo e($user->name); ?> <?php echo e($user->family); ?>  </h3></center>
            </div>
            <!--<div class="profile_info">-->


            <!--</div>-->
        </div>
        <!-- /menu profilex quick info -->

        <!--<br/>-->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                
                <ul class="nav side-menu">
                        <li><a><i class="fa fa-sliders"></i> اسلایدر <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo e(url('admin/product/list')); ?>">لیست اسلایدر</a></li>
                            </ul>
                        </li>


                    <br>
                    <br>
                    <br>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" onclick="return confirm('آیا برای خروج مطمئن هستید.');" data-placement="top" title="خروج" href="<?php echo e(url('admin/logout')); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
