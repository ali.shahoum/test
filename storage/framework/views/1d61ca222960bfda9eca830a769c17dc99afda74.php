<?php $__env->startSection('title'); ?>
    <?php if($setting->contact_title_seo==null): ?>
        <?php echo e($setting->title); ?>

    <?php else: ?>
        <?php echo e($setting->contact_title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php echo e(@$setting->conatct_description_seo); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/images/setting/contact_banner.png')); ?>);margin-top: 60px;">
            <!--<h2>تماس با ما</h2>-->
        </div>

        <div class="container">
            <div class="row" >
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Contact Area Info Start ##### -->
    <div class="contact-area-info section-padding-0-100" >
        <div class="container">
            <br>
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> عضویت / ورود </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <!-- Contact Thumbnail -->
                
                
                
                
                

                <div class="col-12 col-md-5">
                    <!-- Section Heading -->
                    <div class="section-heading">
                        <center>عضویت</center>
                    </div>
                    <!-- Contact Form Area -->
                    <div class="contact-form-area mb-100" >
                        <form method="POST" action="<?php echo e(URL::action('Site\SiteController@requestDecor')); ?>" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <div class="row" >
                                <div class="col-12" >
                                    <div class="form-group" >
                                        <input type="text" name="family" class="form-control" id="contact-name" style="text-align: right!important;height:55px!important"placeholder="نام و نام خانوادگی" required
                                               oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را وارد کنید')"
                                               oninput="setCustomValidity('')"
                                        >

                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="tel" name="mobile" class="form-control" style="text-align: right!important;height:55px!important" id="contact-subject" minlength="10" maxlength="11" placeholder="تلفن همراه" required
                                               oninvalid="this.setCustomValidity('لطفا شماره همراه خود را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <div class="col-12">

                                    <div class="form-group" style="direction: rtl;">
                                        <input type="email" name="email" style="text-align: right!important;height:55px!important" class="form-control" id="contact-email" placeholder="ایمیل" required
                                               oninvalid="this.setCustomValidity('لطفا ایمیل خود را وارد کنید')"
                                               oninput="setCustomValidity('')">

                                    </div>
                                </div>
                                <div class="col-12">

                                    <div class="form-group" style="direction: rtl;">
                                        <input type="password" name="password" style="text-align: right!important;height:55px!important" class="form-control" id="contact-email" placeholder="رمز عبور" required
                                               oninvalid="this.setCustomValidity('لطفا رمز ورود خود را وارد کنید')"
                                               oninput="setCustomValidity('')">

                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn alazea-btn mt-15" style="background-color: #577e3b" >عضویت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <!-- Section Heading -->
                    <div class="section-heading">
                        <center>ورود</center>
                        <p></p>
                    </div>
                    <!-- Contact Form Area -->
                    <div class="contact-form-area mb-100" >
                        <form method="POST" action="<?php echo e(URL::action('Site\SiteController@requestDecor')); ?>" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="row" >
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="tel" name="mobile" class="form-control" style="text-align: right!important;height:55px!important" id="contact-subject" minlength="10" maxlength="11" placeholder="تلفن همراه" required
                                               oninvalid="this.setCustomValidity('لطفا شماره همراه خود را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" style="text-align: right!important;height:55px!important" id="contact-subject" minlength="10" maxlength="11" placeholder="رمز عبور" required
                                               oninvalid="this.setCustomValidity('لطفا رمز عبور خود را وارد کنید')"
                                               oninput="setCustomValidity('')">
                                    </div>
                                </div>


                                <div class="col-12">
                                    <button type="submit" class="btn alazea-btn mt-15" style="background-color: #577e3b" >ورود</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- ##### Contact Area Info End ##### -->

    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row align-items-center justify-content-between">

                <div class="col-12 col-lg-12">
                    <!-- Google Maps -->
                    <div class="map-area mb-100">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1618.266845197517!2d51.48923932797343!3d35.7868293181662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e0562ce7c3153%3A0xee300eaded44ca71!2z2qnYp9ix2K7Yp9mG2Ycg2YbZiNii2YjYsduMINmH2KfbjCDZiNuM!5e0!3m2!1sen!2sus!4v1588457045497!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>