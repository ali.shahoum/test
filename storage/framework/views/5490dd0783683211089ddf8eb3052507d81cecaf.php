<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small>تصاویر بیشتر</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <h3><?php echo e(@$services->title); ?></h3>
                    <form method="POST" action="<?php echo e(URL::action('Admin\ServicesController@postMoreImage')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <br><br>
                        <div class="panel panel-info">
                            <div class="panel-heading">افزودن تصویر</div>

                            <div class="panel-body">
                                <div class="col-md-3">

                                    <input  name="parent" type="hidden" value="<?php echo e(@$id); ?>"  class="form-control"><br>
                                </div>

                                
                                <div class="col-md-5">

                                    <input  name="image" type="file" ><br>
                                </div>
                                <div class="col-md-2">
                                    <button id="singlebutton" name="singlebutton" class="btn btn-success">افزودن</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form method="POST" action="<?php echo e(URL::action('Admin\ServicesController@postDeleteImage')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>



                        <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
                                        </center>
                                        
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">تصویر</th>
                                    <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>


                                </tr>
                                </thead>

                                <tbody>

                                <?php if(count($image) == 0): ?>
                                    <h1> چیزی یافت نشد</h1>
                                <?php else: ?>
                                    <?php $__currentLoopData = $image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "> <img style="width: 100px " src="<?php echo e(asset('assets/images/services/'.$row->image)); ?>"></td>

                                            <td><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>


                                </tbody>
                                <center> <?php if(count($image)): ?> <?php echo $image->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                            </table>
                            <td><a href="<?php echo e(url('admin/product/product_list/'.@$product->id)); ?>" class="btn btn-danger">    بازگشت</i>  </a></td>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <script>

        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>