﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ورود به پنل مدیریت</title>     <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo e(asset('assets/admin/img/favicon.png')); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/bootstrap.min.css')); ?>">     <!-- rahweb css -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/rahweb_style.css')); ?>">     <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/font-awesome.min.css')); ?>">     <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/dist/css/AdminLTE.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/dist/fonts/fonts-fa.css')); ?>">
    <script src="<?php echo e(asset('assets/admin/js/jquery.min.js')); ?>"></script>

    <script src="<?php echo e(asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')); ?>"></script>     <!-- jQuery UI 1.11.4 -->
    <script  rel="javascript" type="text/javascript"  src="<?php echo e(asset('assets/admin/js/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/admin/bootstrap/js/bootstrap.min.js')); ?>"></script>     <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/toastr.css')); ?>">
    <script src="<?php echo e(asset('assets/admin/js/toastr.js')); ?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->     <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>     <![endif]-->   </head>
<body class="login-page">
<?php

    if(file_exists('assets/uploads/setting/main/'.@$setting->background_login) and @$setting->background_login != null)
        $back = asset('assets/uploads/setting/main/'.@$setting->background_login);
    else
        $back = asset('img/1.jpg');
    if(file_exists('assets/uploads/setting/main/'.@$setting->logo_login) and @$setting->logo_login != null)
        $logo = asset('assets/uploads/setting/main/'.@$setting->logo_login);
    //else
        //$logo = asset('img/admin_logo.png');
?>

<img src="<?php echo e($back); ?>" style="width: 100%;height: 100%;position: fixed;"/>

<div class="login-box" style="direction: rtl; margin:0% 65% auto;position: relative;">
    <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="login-logo" style="position: relative;">
        <img src="<?php echo e(@$logo); ?>" style="width: 40%; padding-top:20%;background: #fff3 content-box;"/>
    </div><!-- /.login-logo -->
    <div class="login-box-body"><p class="login-box-msg">پنل مدیریت پادییو</p>

        <form method="post" action="<?php echo e(URL::action('Auth\LoginController@postLogIn1')); ?>" id="form_rahweb">
            <?php echo e(csrf_field()); ?>

            <div class="form-group has-feedback">
                <input type="text" id="email" name="email" class="form-control" placeholder="ایمیل" autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" name="password" class="form-control" placeholder="رمز ورود">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group row">
                <div class="col-md-2" style="margin-top: 9px;">
                    <a href="javascript:void(0)" onclick="refreshCaptcha()">
                        <i class="fa fa-refresh"></i>
                    </a>
                </div>
                <div class="col-md-5 refereshrecapcha">
                    <?php echo \Mews\Captcha\Facades\Captcha::img(); ?>

                </div>
                <div class="col-md-5 ">
                    <input type="captcha" id="captcha" name="captcha" class="form-control" placeholder="کد امنیتی"></div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">ورود</button>
                </div><!-- /.col -->
                <div class="col-xs-4"></div><!-- /.col -->
                <div class="col-xs-4"></div><!-- /.col -->
            </div>
        </form>
        
</div><!-- /.login-box -->
<script>
    function refreshCaptcha() {
            $.ajax({
            url: "<?php echo e(asset('refereshcapcha')); ?>",
            type: 'get',
            dataType: 'html',
            success: function (json) {
                $('.refereshrecapcha').html(json);
            }, error: function (data) {
                alert('مجدد تلاش کنید.');
            }
        });
    }
</script>
</body>
</html>






