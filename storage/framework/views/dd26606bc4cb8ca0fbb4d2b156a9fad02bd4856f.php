
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

    <form method="POST" action="<?php echo e(url('admin/product/edit')); ?>" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
            <div class="panel panel-info">
                <div class="panel-heading"><h2>ویرایش اسلایدر</h2></div>

                <div class="panel-body">
                    <div class="col-md-10">

        <label >موضوع</label>
        <input  name="title" type="text"  class="form-control" value="<?php echo e($slider->title); ?>">
        <label >زیر موضوع</label>

                        <textarea  name="description" type="text"  class="form-control ckeditor" ><?php echo e($slider->description); ?></textarea>
        <label >تصویر</label><br>
        <td><img style="width: 100px " src="<?php echo e(asset('assets/images/product/'.$slider->image)); ?>"><br></td>
        <input  name="image" type="file" value="<?php echo e($slider->image); ?>">

        <input  name="id_product" type="hidden"  class="form-control" value="<?php echo e($slider->id); ?>"><br>


        <label ><input  name="first_page" type="checkbox"  value="1"<?php if($slider->first_page): ?> checked <?php endif; ?>  class="form-control" >نمایش در صفحه اول</label><br>
        <div class="form-group">
            <div class="col-md-4">
                <br><button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>

    </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>