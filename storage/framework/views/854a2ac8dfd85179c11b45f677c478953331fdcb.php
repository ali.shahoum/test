
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small>تصاویر</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <h3>لیست تصاویر</h3>
                    <form method="POST" action="<?php echo e(URL::action('Admin\UploaderController@postAdd')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <br><br>
                        <div class="panel panel-info">
                            <div class="panel-heading">افزودن تصویر</div>

                            <div class="panel-body">
                        <div class="col-md-3">

                            <input placeholder="نام" name="name" type="text"  class="form-control"><br>
                        </div>

                        
                        <div class="col-md-5">

                            <input  name="image" type="file" ><br>
                        </div>
                        <div class="col-md-2">
                            <button id="singlebutton" name="singlebutton" class="btn btn-success">افزودن</button>
                        </div>
                        </div>
                        </div>
                    </form>
                    <form method="POST" action="<?php echo e(URL::action('Admin\UploaderController@postDelete')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>

                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"> جستجو</button>


                        <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input type="checkbox" name="select_all" value="1" id="select_all">
                                        </center>
                                        
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">نام</th>
                                    <th class="column-title" style="display: table-cell;">لینک تصویر</th>
                                    <th class="column-title" style="display: table-cell;">تصویر</th>
                                   <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>
                                    <th class="column-title" style="display: table-cell;">عملیات</th>


                                </tr>
                                </thead>

                                <tbody>

                                <?php if(count($uploader) == 0): ?>
                                    <h1> چیزی یافت نشد</h1>
                                <?php else: ?>
                                    <?php $__currentLoopData = $uploader; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "><?php echo e($row->name); ?></td>
                                            <td class=" "><a href="<?php echo e(asset('assets/images/upload/'.$row->image)); ?>">لینک عکس</a></td>
                                            <td class=" "> <img style="width: 100px " src="<?php echo e(asset('assets/images/upload/'.$row->image)); ?>"></td>

                                            <td><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></td>
                                            <td><a href="<?php echo e(url('admin/uploader/delete/'.$row->id)); ?>" class="btn btn-danger">    <i class="fa fa-trash"></i>  </a></td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>


                                </tbody>
                                <center> <?php if(count($uploader)): ?> <?php echo $uploader->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                            </table>
                        </div>
                    </form>

            </div>
        </div>
    </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">جستجوی تصویر</h4>
                </div>
                <form method="get" action="<?php echo e(URL::current()); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="search" value="search">

                    <div class="modal-body">
                        <input type="text" class="form-control"  name="name" placeholder=" نام تصویر"><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                        <button type="submit" class="btn btn-primary">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>