<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/article/edit')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش مقالات</h2></div>

                    <div class="panel-body">
                        <div class="col-md-11">
                            <div class="panel panel-warning">
                                <div class="panel-heading"><h2>لینک مقاله در سایت</h2></div>

                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                       <a target="_blank" href="http://padiyoo.com/article_detail/<?php echo e($article->title_id); ?>"><?php if($article->title_id): ?><?php echo e($article->title_id); ?> /www.padiyoo.com <?php else: ?> <?php echo e($article->title); ?> /www.padiyoo.com <?php endif; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <label >موضوع</label>
                            <input  name="title" type="text"  class="form-control" value="<?php echo e($article->title); ?>">
                            <label >پیش گفتار</label>
                            <textarea  name="description_first" type="text" id="content" class="form-control ckeditor"><?php echo e($article->description_first); ?></textarea>
                            <label >توضیحات</label>
                            <textarea  name="description" type="text" id="content2" class="form-control ckeditor"><?php echo e($article->description); ?></textarea>
                            <input  name="id" type="hidden"  class="form-control" value="<?php echo e($article->id); ?>"><br>
                            <label >تصویر</label><br>
                            <td><img style="width: 100px "  <?php if($article->image): ?>src="<?php echo e(asset('assets/images/article/'.$article->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/images/article/article.png')); ?>"<?php endif; ?>><br></td>
                            <input  name="image" type="file" value="<?php echo e($article->image); ?>">



                            <label > URL (در صورت خالی بودن این ایتم نام مقاله جایگزین می شود)</label>
                            <input  name="title_id" type="text" placeholder="به هیچ وجه تکراری نباشد" class="form-control" value="<?php echo e($article->title_id); ?>" ><br>

                            <hr>
                            <label >عنوان سئو</label>
                            <input  name="title_seo" type="text"  class="form-control" value="<?php echo e($article->title_seo); ?>"><br>

                            <label > توضیحات سئو</label>
                            <textarea  name="description_seo" type="text"  class="form-control "><?php echo e($article->description_seo); ?></textarea><br>
<hr>
                            <div class="panel panel-success">
                                <div class="panel-heading"><h2>وضعیت مقاله </h2></div>

                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <select  id="selectbasic" name="status" class="form-control">
                                                <option value="<?php echo e($article->status); ?>"><?php if($article->status===0): ?> در حال ویرایش<?php elseif($article->status===1): ?>در صف  انتشار<?php elseif($article->status===2): ?>منتشر شده<?php endif; ?></option>
                                                <option value="0">در حال ویرایش</option>
                                                <option value="1">در صف  انتشار</option>
                                                <option value="2">منتشر شده</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <br><button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script type="text/javascript">

        CKEDITOR.replace('content', {
            language: 'fa',
            filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
            filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
            filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
            filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>'
        });

    </script>
    <script type="text/javascript">

        CKEDITOR.replace('content2', {
            language: 'fa',
            filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
            filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
            filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
            filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>'
        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>