
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/pod/edit')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <input  name="product_id" type="hidden"   value="<?php echo e($pod->id); ?>">

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش ویدیو</h2></div>

                    <div class="panel-body">
                        <div class="col-md-12">

                            <label >عنوان</label>
                            <input  name="title" type="text"  class="form-control" value="<?php echo e($pod->title); ?>"><br>
                            <label >توضیحات</label>
                            <textarea name="description" type="text"  class="form-control ckeditor" ><?php echo e($pod->description); ?></textarea>  <br>

                            <label >تصویر</label>
                            <td><img style="width: 100px " src="<?php echo e(asset('assets/images/pod/'.$pod->image)); ?>"><br></td>
                            <input  name="image" type="file" value="<?php echo e($pod->image); ?>">

                            <label >لینک </label>
                            <input  name="link" type="text"  class="form-control" value="<?php echo e($pod->link); ?>"><br>
                          <hr>
                            <label > عنوان سئو</label>
                            <input  name="title_seo" type="text"  class="form-control" value="<?php echo e($pod->title_seo); ?>"><br>
                            <label >توضیحات سئو</label>
                            <textarea name="description_seo" type="text"  class="form-control " ><?php echo e($pod->description_seo); ?></textarea>  <br>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>            </div>
            </div>


    </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>