
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(URL::action('Admin\AdminsController@postAddGroup')); ?>" id="padiyoo_form">
            <?php echo e(csrf_field()); ?>

            <div class="box-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>عنوان:</label>
                            <input class="form-control" type="text" id="name" name="name"
                                   placeholder="عنوان  را وارد کنید . . ." required>
                        </div>
                        <div class="col-md-6" style="margin-top: 25px;">
                            <input type="checkbox" name="select_all" value="1" id="select_all">
                            <span class="text">انتخاب همه</span>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    <?php
                    if(isset($data->permission)){
                        $accessDB = unserialize($data->permission);
                    }else{
                        $accessDB = [];
                    }
                    ?>
                    <?php $__currentLoopData = Config::get('site.permisions'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="widget col-md-6" style="background-color: rgb(248, 248, 248); border: medium solid;">
                            <div class="widget-header bordered-bottom bordered-themesecondary">
                                <i class="widget-icon fa fa-unlock-alt themesecondary"></i>
                                <span class="widget-caption themesecondary" style="color: #3c8dbc;"><?php echo e($value['title']); ?></span>
                            </div>
                            <!--Widget Header-->
                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <div class="tickets-container" style="height: 150px;">
                                        <?php $__currentLoopData = $value['access']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyAccess => $access): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                            $check = 0;
                                            if(isset($accessDB[$key][$keyAccess])) {
                                                $check = 1;
                                            }
                                            ?>
                                            <div class="col-md-6">
                                                <input  type="checkbox"  name="access[<?php echo e($key); ?>][<?php echo e($keyAccess); ?>]" value="1" <?php if($check): ?> checked <?php endif; ?>>
                                                <span class="text"><?php echo e($access); ?></span>

                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                        <div class="checkbox" style="display: none;">
                                            <label style="padding-left: 0px;">
                                                <input  type="checkbox"  name="access[user][changePassword]" value="1" checked>
                                            </label>
                                        </div>

                                        <br/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>



                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">ذخیره</button>
                </div>
                </div>
        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <script>

        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });

        (function($,W,D)
        {
            var JQUERY4U = {};

            JQUERY4U.UTIL =
                {
                    setupFormValidation: function()
                    {
                        //form validation rules
                        $("#padiyoo_form").validate({
                            rules: {
                                name: "required",
                                agree: "required"
                            },
                            messages: {
                                name: "این فیلد الزامی است."
                            },
                            submitHandler: function(form) {
                                form.submit();
                            }
                        });
                    }
                }

            //when the dom has loaded setup form validation rules
            $(D).ready(function($) {
                JQUERY4U.UTIL.setupFormValidation();
            });

        })(jQuery, window, document);
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>