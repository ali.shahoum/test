<?php $__env->startSection('title'); ?>
    <?php if($ads->title_seo==null): ?>
        <?php echo e($ads->title); ?>

    <?php else: ?>
        <?php echo e($ads->title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php if($ads->description_seo == null): ?>
        <?php echo e($ads->description); ?>

    <?php else: ?>
        <?php echo e($ads->description_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <link href="<?php echo e(asset('assets/site/bootstrap.min.css')); ?>" rel="stylesheet" id="bootstrap-css" >
    <script src="<?php echo e(asset('assets/site/css/js/jquery-1.11.1.min.js')); ?>"></script>

    
    
    
    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(asset('assets/images/services/ads.png')); ?>);margin-top: 60px;">
            <h2></h2>
        </div>

        <div class="container">

        </div>
    </div>


    <div class="container">

        
            
        <div class="row" style="direction: rtl!important;">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading clickable">
                        <h3 class="panel-title">
                            Panel 2</h3>
                        
                    </div>
                    <div class="panel-body">
                        Panel content <a href="http://www.jquery2dotnet.com/2013/11/cool-big-social-counter-button-with.html">Cool Big Social Counter Button With Bootstrap</a></div>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>