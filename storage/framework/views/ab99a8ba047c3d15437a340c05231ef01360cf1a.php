
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

    <form method="POST" action="<?php echo e(url('admin/gallery/add')); ?>" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>


        <div class="form-group">
            <div class="panel panel-info">
                <div class="panel-heading"><h2>افزودن تصویر</h2></div>

                <div class="panel-body">
            <div class="col-md-10">

        <label >موضوع</label>
        <input  name="name" type="text"  class="form-control"><br>

        <label >عکس</label>
        <input  name="image" type="file" ><br>

        

                <button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
            </div>
        </div>
        </div>
        </div>

    </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>