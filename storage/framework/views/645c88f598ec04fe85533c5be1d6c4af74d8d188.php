<slider class="hero-area" >
    

        
        
        
            
            
            
                
                    
                        
                        
                            

                            
                                
                                
                            
                        
                    
                
            
        

        

    
    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <?php $s = 0; ?>
            <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li data-target="#demo" data-slide-to="<?php echo e($key); ?>" class="<?php if($s == 0 ){ echo "active"; }?>"></li>
                    <?php $s++; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <?php $s = 0; ?>
        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="carousel-item <?php if($s == 0 ){ echo "active"; }?>">
                <img src="<?php echo e('assets/images/slider/'.$row->image); ?>" alt="Los Angeles" width="100%" >
            </div>
                    <?php $s++; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
                
            
                
            
            
                
            
            
                
            
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
</slider>
