<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    
    
    
    <link rel="shortcut icon" type="image/x-icon"
          
          href="<?php echo e(asset('assets/uploads/uploader/setting/medium/'.@$setting->fav)); ?>"
    >
    <title>   پنل  پادییو </title>

    <link href="<?php echo e(asset('assets/admin/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/bootstrap-rtl.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/nprogress.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/green.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/daterangepicker.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/profile/css/custom.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/profile/css/style.css')); ?>" rel="stylesheet">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/toastr.css')); ?>">


    <!-- CKEditor -->

    <script src="<?php echo e(asset('assets/editor/ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/admin/js/sorttable.js')); ?>"></script>

</head>
