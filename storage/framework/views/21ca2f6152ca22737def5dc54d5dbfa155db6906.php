    
        
    
        
    



    
        
    
        
    

<?php $__env->startSection('content'); ?>
    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/site/image/bg-img/art.png')); ?>);">

        </div>

        <div class="container">

        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <br><br>
    <!-- ##### Blog Content Area Start ##### -->
    <section class="blog-content-area section-padding-0-100" style="text-align: right;direction: rtl">
        <div class="container">
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e('/loge'); ?>">  غرفه ها </a></li>
                            <li class="breadcrumb-item active" aria-current="page">   <?php echo e($loge->title); ?>  </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- Blog Posts Area -->
                <div class="col-12 col-md-11">
                    <div class="blog-posts-area">

                        <!-- Post Details Area -->
                        <div class="single-post-details-area">
                            <div class="post-content">
                                <h4 class="post-title">   <?php echo e($loge->title); ?>  </h4>
                                <div class="post-meta mb-30">
                                    
                                    
                                </div>
                                <div class="post-thumbnail mb-30">
                                    <img src="<?php echo e(asset('assets/images/loge/'.$loge->image)); ?>" alt="">
                                </div>
                         <p><?php echo $loge->description; ?></p>
                            </div>
                        </div>

                        <!-- Post Tags & Share -->
                        <div class="post-tags-share d-flex justify-content-between align-items-center">

                        </div>

                        <!-- Comment Area Start -->
                        <div class="comment_area clearfix">



                        </div>

                        <!-- Leave A Comment -->
                        <div class="leave-comment-area clearfix">
                            <div class="comment-form">

                            </div>
                        </div>

                    </div>
                </div>

                <!-- Blog Sidebar Area -->
                <div class="col-12 col-sm-9 col-md-1">
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Blog Content Area End ##### -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>