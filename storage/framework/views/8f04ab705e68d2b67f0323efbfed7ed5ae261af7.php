<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<?php echo e(asset('assets/admin/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/bootstrap-rtl.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/nprogress.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/green.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/daterangepicker.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/custom.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/admin/css/style.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('assets/admin/css/toastr.css')); ?>">

</head>
