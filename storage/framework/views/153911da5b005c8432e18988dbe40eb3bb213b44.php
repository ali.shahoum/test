
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small>کاربران</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <h3>لیست مدیران</h3>
                    <form method="POST"  action="<?php echo e(URL::action('Admin\AdminsController@postDelete')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>
                        <div class="btn-group">
                            <a type="button" href="<?php echo e(url('admin/users/add_admin')); ?>" class="btn btn-success">افزودن مدیر</a>
                            
                                    
                                
                            
                            
                                
                                

                                
                            
                        </div>

                        <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input type="checkbox" name="select_all" value="1" id="select_all">
                                        </center>
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">نام خانوادگی</th>
                                    <th class="column-title" style="display: table-cell;">ایمیل</th>
                                    <th class="column-title" style="display: table-cell;">تصویر</th>


                                    
                                    <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>
                                    <th class="column-title" style="display: table-cell;">عملیات</th>


                                </tr>
                                </thead>

                                <tbody>

                                
                                    
                                

                                    <?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "><?php echo e($row->family); ?></td>
                                            <td class=" "><?php echo e($row->email); ?></td>
                                            <td class=" ">
                                                <img style="width: 100px;height:70px " <?php if($row->image): ?>src="<?php echo e(asset('assets/images/users/'.$row->image)); ?>"<?php else: ?> src="<?php echo e(asset('img/users.png')); ?>"<?php endif; ?>>
                                                <br></td>
                                            
                                            <td>
                                                <?php if($row->id===$userId or $row->status!=2): ?>
                                                <?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?>

                                                <?php else: ?> <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if($row->id===$userId or $row->status!=2): ?>
                                                <a href="<?php echo e(url('admin/users/delete1/'.$row->id)); ?>" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');" class="btn btn-danger "><i class="fa fa-trash-o"></i>    </a>
                                                <a href="<?php echo e(url('admin/admin/edit/'.$row->id)); ?>"  class="btn btn-success "><i class="fa fa-pencil-square-o"></i>    </a>
                                                  <?php else: ?> <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                


                                </tbody>

                            </table>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    
        
            
                
                    
                    
                
                
                    
                    

                    
                        
                        
                    
                    
                        
                        
                    
                
            
        
    


<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select_all").on('click', function () {
                $.each($("input"), function (index, value) {
                    if (value.type == 'checkbox') {
                        value.checked = $("#select_all")[0].checked;
                    }
                });
            });
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>