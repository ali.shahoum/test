<footer class="footer-area bg-img " style="background-image: url(<?php echo e(url('assets/site/image/bg-img/logo-footer.jpg')); ?>);">
    <!-- Main Footer Area -->
    <div class="main-footer-area" >
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget" style="text-align: right!important;direction: rtl">
                        <div class="widget-title">
                            <center><h6 style="color: #ffffff">خدمات پادییو</h6></center>
                        </div>
<br>
                        <!-- Single Best Seller Products -->
                        <?php $__currentLoopData = $ser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="single-best-seller-product d-flex align-items-center">
                                <div class="product-thumbnail">
                                    <a href="<?php echo e(url(''.$row->kind)); ?>"><img style="border-radius:50%!important;height: 45px;" src="<?php echo e(asset('assets/images/services/'.$row->image)); ?>" alt=""></a>
                                </div>
                                <div class="product-info"  style="margin-right: 10px">
                                    <a href="<?php echo e(url(''.$row->kind)); ?>"><?php echo e($row->title); ?></a>
                                </div>
                            </div><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <!-- Single Best Seller Products -->

                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3" >
                    <div class="single-footer-widget">
                        <div class="footer-logo mb-30" style="margin-bottom: 0px!important;">
                        </div>
                        <center>
                            <a href="<?php echo e(url('/home')); ?>"><img src="<?php echo e(asset('assets/images/setting/'.@$setting->logo)); ?>" alt="" style="width: 180px"></a>
                        </center><br>
                        <div class="social-info" style="text-align: center">
                            
                            
                            
                            <a href="http://www.instagram.com/<?php echo e(@$setting->instagram); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="http://www.linkedin.com/<?php echo e(@$setting->linkedin); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a href="http://www.twitter.com/<?php echo e(@$setting->twitter); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="mailto:/<?php echo e(@$setting->email); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3" >
                    <div class="single-footer-widget">
                        <div class="widget-title">
                            <center><h6 style="color: #ffffff">تماس با ما</h6></center>
                        </div>
<br>
                        <div class="contact-information" style="direction: rtl;float: right">
                            <p style=" text-align: justify;text-justify: inter-word;"><span style="color: #ffffff">آدرس: </span>  <?php echo e(@$setting->address); ?></p>
                            <p style="text-align: center"><span style="color: #ffffff"> تلفن: </span><a href="tel:<?php echo e(@$setting->tel); ?>" style="color: #ffffff"><?php echo e(@$setting->tel); ?></a></p>
                            <p style="text-align: center"><span style="color: #ffffff">ایمیل: </span><a href="mailto:/<?php echo e(@$setting->email); ?>" style="color: #ffffff"> <?php echo e(@$setting->email); ?></a></p><br>
                            
                            
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->


                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget">
                        <!-- Section Heading -->
                        <div class="section-heading">
                            <center><h6 style="color: #ffffff">درخواست مشاوره </h6></center>
                            <p></p>
                        </div>
                        <!-- Contact Form Area -->
                        <div class="contact-form-area mb-100" >
                            <form method="POST" action="<?php echo e(URL::action('Site\SiteController@requestDecor')); ?>" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="row" >
                                    <div class="col-12 col-md-6">
                                        <div class="form-group" >
                                            <input type="tel" name="mobile" minlength="10" maxlength="11" class="form-control" style="text-align: right!important;font-size: 12px!important;" id="contact-subject" placeholder="تلفن همراه" required
                                                   oninvalid="this.setCustomValidity('لطفا شماره همراه خود را وارد کنید')"
                                                   oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6" style="!important;">
                                        <div class="form-group" >
                                            <input type="text" name="family" class="form-control" id="contact-name" style="text-align: right!important;font-size: 12px!important;"placeholder=" نام خانوادگی" required
                                                   oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را وارد کنید')"
                                                   oninput="setCustomValidity('')">

                                        </div>
                                    </div>
                                    <div class="col-12">

                                        <div class="form-group" style="direction: rtl">
                                            <input type="email" name="email" style="text-align: right!important;font-size: 12px!important;" class="form-control" id="contact-email" placeholder="ایمیل" required
                                                   oninvalid="this.setCustomValidity('لطفا ایمیل خود را وارد کنید')"
                                                   oninput="setCustomValidity('')">

                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <select style="direction: rtl;padding: 0px!important;font-size: 12px!important;" id="selectbasic" name="subject" class="form-control" required>
                                                <option value="" disabled="" selected="" style="display: none;font-size: 12px!important;">موضوع مشاوره</option>
                                                <option value="0">بالکن</option>
                                                <option value="1">پاسیو</option>
                                                <option value="2">بام</option>
                                                <option value="3">دیوار</option>
                                                <option value="4">فضای داخلی</option>
                                                <option value="5">چاپ و تبلیغات</option>
                                                <option value="6">طراحی غرفه و سازه های نمایشگاهی</option>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn alazea-btn mt-15" style="background-color: #577e3b;height: 35px;min-width: 70px!important;" >ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>


</footer>

