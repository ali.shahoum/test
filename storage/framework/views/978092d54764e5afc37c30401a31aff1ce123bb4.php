<?php $__env->startSection('content'); ?>

    <div class="right_col" role="main">
        <div class="panel panel-success">
            <div class="panel-heading"><h2>پروژه های انجام شده</h2></div>

            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                        <a type="button" href="<?php echo e(url('admin/services/add_loges')); ?>" class="btn btn-success"> غرفه ها</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="<?php echo e(url('admin/services/edit_loges')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش متن غرفه ها </h2></div>

                    <div class="panel-body">
                        <div class="col-md-11">
                            <input  name="kind" type="hidden"  class="form-control" value="loges"><br>
                            <input  name="id" type="hidden"  class="form-control" value="<?php echo e(@$loges->id); ?>"><br>
                            <input  name="status" type="hidden"  class="form-control" value="services"><br>
                            <label >موضوع</label>
                            <input  name="title" type="text"  class="form-control" value="<?php echo e(@$loges->title); ?>">
                            <label >توضیحات</label>
                            <textarea  name="description" type="text" id="content" class="form-control ckeditor"><?php echo e(@$loges->description); ?></textarea>
                            <label >تصویر</label><br>
                            <td><img style="width: 100px "  <?php if(@$loges->image): ?>src="<?php echo e(asset('assets/images/services/'.@$loges->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/images/article/article.png')); ?>"<?php endif; ?>><br></td>
                            <input  name="image" type="file" value="<?php echo e(@$loges->image); ?>">
                            <hr>
                            <label >موضوع سئو</label>
                            <input  name="title_seo" type="text"  class="form-control" value="<?php echo e($loges->title_seo); ?>"><br>

                            <label > توضیحات سئو</label>
                            <textarea  name="description_seo" type="text"  class="form-control "><?php echo e($loges->description_seo); ?></textarea><br>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <br><button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script  type="text/javascript">

        CKEDITOR.replace('content', {
            // language: 'fa',
            filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
            filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
            filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
            filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>',

        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>