<section class="parallax">
    <div class="container" style="padding-top: 5%">
        <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
                <h4>
                    انواع سازه های دکوراسیون سبز
                </h4>
                <p></p>
            </div>
        </div>
        <div class="row justify-content-center" style="text-align: center">

    <!-- Single Product Area -->
    <div class="col-12 col-sm-6 col-lg-3">
        <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms">
            <!-- Product Image -->
            <div >
                <a href="<?php echo e('/roof'); ?>"><img src="<?php echo e(asset('assets/innerDecor/roof.jpeg')); ?>" alt=""></a>
                <!-- Product Tag -->
                
                
                
                
                
                
                
                
            </div>
            <!-- Product Info -->
            <div class="product-info mt-15 text-center">
                <a href="<?php echo e('/roof'); ?>">
                    <p>بام سبز</p>
                </a>
                
            </div>
        </div>
    </div>

    <!-- Single Product Area -->
    <div class="col-12 col-sm-6 col-lg-3">
        <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="200ms">
            <!-- Product Image -->
            <div class="">
                <a href="<?php echo e('/pasio'); ?>"><img src="<?php echo e(asset('assets/innerDecor/passio.jpeg')); ?>" alt=""></a>
                
                
                
                
                
            </div>
            <!-- Product Info -->
            <div class="product-info mt-15 text-center">
                <a href="<?php echo e('/pasio'); ?>">
                    <p>پاسییو سبز</p>

                </a>
                
            </div>
        </div>
    </div>

    <!-- Single Product Area -->
    <div class="col-12 col-sm-6 col-lg-3">
        <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="300ms">
            <!-- Product Image -->
            <div class="">
                <a href="<?php echo e('/wall'); ?>"><img src="<?php echo e(asset('assets/innerDecor/wall.jpeg')); ?>" alt=""></a>
                
                
                
                
                
            </div>
            <!-- Product Info -->
            <div class="product-info mt-15 text-center">
                <a href="<?php echo e('/wall'); ?>">
                    <p>دیوار سبز</p>

                </a>
                
            </div>
        </div>
    </div>

    <!-- Single Product Area -->
    <div class="col-12 col-sm-6 col-lg-3">
        <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="400ms">
            <!-- Product Image -->
            <div class="">
                <a href="<?php echo e('/balkon'); ?>"><img src="<?php echo e(asset('assets/innerDecor/balkon.jpeg')); ?>" alt=""></a>
                <!-- Product Tag -->
                <div class="product-tag sale-tag">
                    
                </div>
                
                
                
                
                
            </div>
            <!-- Product Info -->
            <div class="product-info mt-15 text-center">
                <a href="<?php echo e('/balkon'); ?>">
                    <p>بالکن سبز</p>

                </a>
                
            </div>
        </div>
    </div>

    
    
    

</div>
</div>
</section>
<style>
    .parallax {
        /* The image used */
        background-image: url(<?php echo e(url('assets/site/image/bg-img/bb.png')); ?>);

        /* Full height */
        height: 100%;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
