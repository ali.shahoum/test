<section class="our-services-area bg-gray section-padding-100-0 parallax">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h4>آخرین پادکست </h4>
                    <p></p>
                </div>
            </div>
        </div>

        <div class="row justify-content-between">

            <div class="col-12 col-lg-6" >
                <div class="alazea-video-area bg-overlay mb-100">
                    <img style="width: 100%;height: 350px!important;" <?php if(@$pod->image): ?> src="<?php echo e(asset('assets/images/pod/'.$pod->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/site/image/bg-img/koko.jpg')); ?>"<?php endif; ?> alt="">
                    <a href="<?php echo e(@$pod->link); ?>" class="video-icon">
                        <i class="fa fa-play" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="alazea-service-area mb-100">


                    <!-- Single Service Area -->
                    <div class="single-service-area d-flex align-items-center wow fadeInUp" data-wow-delay="100ms" style="text-align: right !important;">
                        <!-- Icon -->

                        <!-- Content -->
                        <div class="service-content">
                            <center><h5><?php echo e(@$pod->title); ?></h5></center>
                            <p style="padding-right: 18px">
                                <?php echo @$pod->description; ?>

                            </p>
                        </div>
                        <div class="service-icon mr-30">
                            <img src="<?php echo e(asset('assets/site/css/core-img/s1.png')); ?>" alt="">
                        </div>
                    </div>



                </div>
            </div>

        </div>
    </div>
</section>
<style>
    .parallax {
        /* The image used */
        background-image: url(<?php echo e(url('assets/site/image/bg-img/bb.png')); ?>);

        /* Full height */
        height: 100%;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
<!-- ##### Service Area End ##### -->
