
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('layout.admin.blocks.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="right_col" role="main">

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>افزودن محصول
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="productForm" >
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">نام محصول</label>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="نام محصول">
                                </div>

                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>افزودن زمان بندی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="timeForm" >
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">انتخاب</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="cat" class="form-control" id="cat">
                                        <option>انتخاب زمان</option>
                                        <option value="1">صبح</option>
                                        <option value="2">ظهر</option>
                                        <option value="3">عصر</option>
                                        <option value="4">شب</option>
                                        
                                        
                                        
                                    </select>
                                </div>
                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> افزودن  قیمت محصول در هر زمان بندی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <p></p>
                    <!-- start pop-over -->
                    <div class="bs-example-popovers">
                        <form id="priceForm" >
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <select name="product_id" class="form-control" id="product_id">
                                        <option>انتخاب محصول</option>
                                        <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($row1->id); ?>" > <?php echo e($row1->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <select name="time_sort" class="form-control" id="time_sort">
                                        <option value="">انتخاب زمان</option>
                                        <?php $__currentLoopData = $time; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($row->id); ?>" > <?php if($row->cat==4): ?>شب <?php elseif($row->cat==1): ?>صبح  <?php elseif($row->cat==2): ?> ظهر <?php else: ?> عصر <?php endif; ?> </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" name="price" id="price" class="form-control" placeholder="قیمت محصول">
                                </div>

                                <button type="submit" id="submit" class="btn btn-success">ثبت</button>
                            </div>
                        </form>
                    </div>
                    <!-- end pop-over -->

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>جدول محصولات
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <center>
                                    <input type="checkbox" name="select_all" value="1" id="select_all">
                                </center>
                                
                            </th>
                            <th>نام محصول</th>
                            <th>قیمت صبح</th>
                            <th>قیمت ظهر</th>
                            <th>قیمت عصر</th>
                            <th>قیمت شب</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="even pointer">

                            <td class="a-center ">
                                <center>
                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                           type="checkbox"
                                           value="<?php echo e($row->id); ?>"/>

                                </center>
                                
                            </td>
                            <td class=" "><?php echo e($row->name); ?></td>
                            <td class=" "><?php echo e($row->price1); ?></td>
                            <td class=" "><?php echo e($row->price2); ?></td>
                            <td class=" "><?php echo e($row->price3); ?></td>
                            <td class=" "><?php echo e($row->price4); ?></td>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script type="text/javascript">

        $('#productForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            name = $('#name').val();

            $.ajax({
                url: "product/create",
                type:"POST",
                data:{
                    "_token": "<?php echo e(csrf_token()); ?>",
                    name:name,
                },
                success:function(response){
                    $("#productForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.info(response);
                    },

            });
        });

</script>

        <script>

        $('#timeForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            cat = $('#cat').val();

            $.ajax({
                url: "product/time",
                type:"POST",
                data:{
                    "_token": "<?php echo e(csrf_token()); ?>",
                    cat:cat,
                },
                success:function(response){
                    $("#timeForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.success(response);
                    },
                error: function (response)
                {
                    toastr.warning('این زمانبندی وجود دارد');
                }
            });
        });
    </script>
    <script type="text/javascript">

        $('#priceForm').on('submit',function(event){
            event.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            id = $('#product_id').val();
            time_sort = $('#time_sort').val();
            price = $('#price').val();

            $.ajax({
                url: "product/price",
                type:"POST",
                data:{
                    "_token": "<?php echo e(csrf_token()); ?>",
                    id:id,
                    time_sort:time_sort,
                    price:price,
                },
                success:function(response){
                    $("#priceForm").trigger("reset"); //reset form
                    window.location.reload();
                    toastr.info(response);
                },

            });
        });

    </script>
            <script type="text/javascript">
            $(document).ready(function () {
                $("#select_all").on('click', function () {
                    $.each($("input"), function (index, value) {
                        if (value.type == 'checkbox') {
                            value.checked = $("#select_all")[0].checked;
                        }
                    });
                });
            });
    </script>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>