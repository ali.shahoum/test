
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/setting/edit')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>




            <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab1" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">تنظیمات سایت</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false"> درباره ما</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content33" role="tab" id="profile-tabb3" data-toggle="tab" aria-controls="profile" aria-expanded="false">تماس با ما</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content34" role="tab" id="profile-tabb3" data-toggle="tab" aria-controls="profile" aria-expanded="false">ویدیو</a>
                                </li> <li role="presentation" class=""><a href="#tab_content35" role="tab" id="profile-tabb3" data-toggle="tab" aria-controls="profile" aria-expanded="false">رمز عبور</a>
                                </li>
                            </ul>
                            <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content11" aria-labelledby="home-tab">
                                    <div class="col-md-12">

                                        <div class="panel panel-info">
                                            <div class="panel-body">

                                                <input  name="id_product" type="hidden"  class="form-control" value="<?php echo e($setting->id); ?>"><br>
                                                <label style="padding: 15px"><input  name="site" type="checkbox"  value="1"<?php if($setting->site): ?> checked <?php endif; ?>  class="form-control"  >فعال بودن سایت    </label><hr>

                                                <label >عنوان سایت</label>
                                                <input  name="title" type="text"  class="form-control " value="<?php echo e($setting->title); ?>">
                                                 <label >توضیحات سایت</label>
                                                <input  name="description_seo" type="text"  class="form-control " value="<?php echo e($setting->description_seo); ?>">

                                                <hr><label >عنوان سئو مقالات </label>
                                                <input  name="article_title_seo" type="text"  class="form-control" value="<?php echo e(@$setting->article_title_seo); ?>">
                                                <label > توضیحات سئو مقالات</label>
                                                <textarea  name="article_description_seo" type="text"  class="form-control "><?php echo e($setting->article_description_seo); ?></textarea>

                                                <hr><label >عنوان سئو گالری </label>
                                                <input  name="gallery_title_seo" type="text"  class="form-control" value="<?php echo e(@$setting->gallery_title_seo); ?>">
                                                <label > توضیحات سئو گالری</label>
                                                <textarea  name="gallery_description_seo" type="text"  class="form-control "><?php echo e($setting->gallery_description_seo); ?></textarea>

                                                <hr><label >عنوان سئو پادکست </label>
                                                <input  name="pod_title_seo" type="text"  class="form-control" value="<?php echo e(@$setting->pod_title_seo); ?>">
                                                <label > توضیحات سئو پادکست</label>
                                                <textarea  name="pod_description_seo" type="text"  class="form-control "><?php echo e($setting->pod_description_seo); ?></textarea>

                                                <hr><label >تصویر لوگو </label><br>
                                                <td><img style="width: 100px " src="<?php echo e(asset('assets/images/setting/'. @$setting->logo)); ?>"><br></td>
                                                <input  name="logo" type="file" ><hr>

                                                
                                                
                                                
                                                <hr><label >fav لوگو </label><br>
                                                <td><img style="width: 100px " src="<?php echo e(asset('assets/uploads/uploader/setting/medium/'. @$setting->fav)); ?>"><br></td>
                                                <input  name="fav" type="file" ><hr>
                                                
                                                
                                                
                                                
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="panel panel-info">

                                            <div class="panel-body">

                                                <label > متن</label>
                                                <textarea  name="about_us" type="text" id="content" class="form-control ckeditor" ><?php echo e(@$setting->about_us); ?></textarea>

                                                <hr>
                                                      <label >عنوان سئو درباره ما </label>
                                                <input  name="about_title_seo" type="text"  class="form-control" value="<?php echo e(@$setting->about_title_seo); ?>">
                                                <label > توضیحات سئو درباره ما</label>
                                                <textarea  name="about_description_seo" type="text"  class="form-control "><?php echo e($setting->about_description_seo); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content33" aria-labelledby="profile-tab">
                                    <div class="col-md-12">
                                        <div class="panel panel-info">

                                            <div class="panel-body">

                                                <label >شماره تماس </label>
                                                <input  name="tel" type="text"  class="form-control" value="<?php echo e(@$setting->tel); ?>">
                                                <label >عنوان سئو تماس با ما </label>
                                                <input  name="contact_title_seo" type="text"  class="form-control" value="<?php echo e(@$setting->contact_title_seo); ?>">
                                                <label > توضیحات سئوتماس با ما</label>
                                                <textarea  name="contact_description_seo" type="text"  class="form-control "><?php echo e($setting->contact_description_seo); ?></textarea><br>
                                                <label >آدرس </label>
                                                <input  name="address" type="text"  class="form-control" value="<?php echo e(@$setting->address); ?>">
                                                <label > ایمیل</label>
                                                <input  name="email" type="text"  class="form-control" value="<?php echo e(@$setting->email); ?>">
                                                <label > ادرس کانال تلگرام</label>
                                                <input  name="telegram" type="text"  class="form-control" value="<?php echo e(@$setting->telegram); ?>">
                                                <label > ادرس کانال اینستاگرام</label>
                                                <input  name="instagram" type="text"  class="form-control" value="<?php echo e(@$setting->instagram); ?>">
                                                <label > ادرس کانال اپارات</label>
                                                <input  name="aparat" type="text"  class="form-control" value="<?php echo e(@$setting->aparat); ?>">
                                                <label > ادرس کانال توییتر</label>
                                                <input  name="twitter" type="text"  class="form-control" value="<?php echo e(@$setting->twitter); ?>">
                                                <label > ادرس کانال لینکین</label>
                                                <input  name="linkedin" type="text"  class="form-control" value="<?php echo e(@$setting->linkedin); ?>">

                                            </div> </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content34" aria-labelledby="profile-tab">
                                    <div class="col-md-12">
                                        <div class="panel panel-info">

                                            <div class="panel-body">

                                                <label >عنوان</label>
                                                <input  name="title_2" type="text"  class="form-control" value="<?php echo e(@$setting->title_2); ?>">
                                                <label >کلیپ</label><br>
                                                <video style="width: 100px " src="<?php echo e(asset('assets/uploads/uploader/setting/video/'. @$setting->video)); ?>"></video><br>
                                                <input  name="video" type="file" >

                                                <label > متن  </label>
                                                <textarea  name="description2" type="text" id="content2" class="form-control ckeditor" ><?php echo e(@$setting->description2); ?>


                                            </textarea>
                                            </div> </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content35" aria-labelledby="profile-tab">
                                    <div class="col-md-12">
                                        <div class="panel panel-info">

                                            <div class="panel-body">

                                                <label >رمز عبور جدید</label>
                                                <input  name="password" type="password"  class="form-control" value="">

                                            </div>
                                            <br>
                                            <br>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <hr>
            <button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>


        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script  type="text/javascript">

        CKEDITOR.replace('content', {
            // language: 'fa',
            filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
            filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
            filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
            filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>',

        });
    </script>
    <script  type="text/javascript">

        CKEDITOR.replace('content2', {
            // language: 'fa',
            filebrowserImageBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Images')); ?>',
            filebrowserImageUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Images&_token=')); ?>',
            filebrowserBrowseUrl: '<?php echo e(asset('/laravel-filemanager?type=Files')); ?>',
            filebrowserUploadUrl: '<?php echo e(asset('/laravel-filemanager/upload?type=Files&_token=')); ?>',

        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>