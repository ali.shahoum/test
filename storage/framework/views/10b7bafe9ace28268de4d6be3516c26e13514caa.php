<?php $__env->startSection('title'); ?>
    <?php if($decor->title_seo==null): ?>
        <?php echo e($decor->title); ?>

    <?php else: ?>
        <?php echo e($decor->title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php if($decor->description_seo == null): ?>
        <?php echo e($decor->description); ?>

    <?php else: ?>
        <?php echo e($decor->description_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(asset('assets/images/services/decor.png')); ?>);margin-top: 60px;">
            <h2></h2>
        </div>

        <div class="container">

        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### About Area Start ##### -->
    <section class="about-us-area">
        <div class="container"><br>
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item" style="color: #577e3b"> خدمات</li>
                            <li class="breadcrumb-item active" aria-current="page"> دکوراسیون سبز داخلی </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-12 col-lg-5">


                </div>


            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <center> <h2><?php echo e(@$decor->title); ?></h2>   </center>

                    <p><?php echo $decor->pre_description; ?></p>
                    <section class="new-arrivals-products-area ">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <!-- Section Heading -->
                                    <div class="section-heading text-center">
                                        <h4>أنواع سازه های دکوراسیون سبز</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <!-- Single Product Area -->
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                                        <!-- Product Image -->
                                        <div class="">
                                            <a href="<?php echo e('/roof'); ?>"><img src="<?php echo e(asset('assets/innerDecor/roof.jpeg')); ?>" alt=""></a>
                                            <!-- Product Tag -->
                                            
                                                
                                            
                                            
                                                
                                                
                                                
                                            
                                        </div>
                                        <!-- Product Info -->
                                        <div class="product-info mt-15 text-center">
                                            <a href="<?php echo e('/roof'); ?>">
                                                <p>بام سبز</p>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>

                                <!-- Single Product Area -->
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="200ms">
                                        <!-- Product Image -->
                                        <div class="">
                                            <a href="<?php echo e('/pasio'); ?>"><img src="<?php echo e(asset('assets/innerDecor/passio.jpeg')); ?>" alt=""></a>
                                            
                                                
                                                
                                                
                                            
                                        </div>
                                        <!-- Product Info -->
                                        <div class="product-info mt-15 text-center">
                                            <a href="<?php echo e('/pasio'); ?>">
                                                <p>پاسییو سبز</p>

                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>

                                <!-- Single Product Area -->
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="300ms">
                                        <!-- Product Image -->
                                        <div class="">
                                            <a href="<?php echo e('/wall'); ?>"><img src="<?php echo e(asset('assets/innerDecor/wall.jpeg')); ?>" alt=""></a>
                                            
                                                
                                                
                                                
                                            
                                        </div>
                                        <!-- Product Info -->
                                        <div class="product-info mt-15 text-center">
                                            <a href="<?php echo e('/wall'); ?>">
                                                <p>دیوار سبز</p>

                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>

                                <!-- Single Product Area -->
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="400ms">
                                        <!-- Product Image -->
                                        <div class="">
                                            <a href="<?php echo e('/balkon'); ?>"><img src="<?php echo e(asset('assets/innerDecor/balkon.jpeg')); ?>" alt=""></a>
                                            <!-- Product Tag -->
                                            <div class="product-tag sale-tag">
                                                
                                            </div>
                                            
                                                
                                                
                                                
                                            
                                        </div>
                                        <!-- Product Info -->
                                        <div class="product-info mt-15 text-center">
                                            <a href="<?php echo e('/balkon'); ?>">
                                                <p>بالکن سبز</p>

                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>

                                
                                    
                                

                            </div>
                        </div>
                    </section>
                    <p><?php echo $decor->description; ?></p>


                </div>
            </div>

        </div>

    </section>
    <div class=" container col-md-9 " style="  border: 1px solid green;margin-bottom: 10%">
        <!-- Section Heading -->
        <div class="section-heading" style="margin-top: 2%">
            <center><h5>
                    فرم درخواست طراحی دکوراسیون داخلی</h5></center>
            <p></p>
        </div>
        <!-- Contact Form Area -->
        <div class="contact-form-area mb-100" >
            <form method="POST" action="<?php echo e(URL::action('Site\SiteController@requestDecor')); ?>" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="row" >

                    <div class="col-12 " >
                        <div class="form-group" >
                            <input type="text" name="family" class="form-control" id="contact-name" style="text-align: right!important;height:55px!important"placeholder="نام و نام خانوادگی" required
                                   oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را وارد کنید')"
                                   oninput="setCustomValidity('')"
                            >

                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="form-group">
                            <input type="tel" name="mobile" minlength="10" maxlength="11" class="form-control" style="text-align: right!important;height:55px!important" id="contact-subject" placeholder="تلفن همراه" required oninvalid="this.setCustomValidity('لطفا شماره همراه خود را وارد کنید')"
                                   oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="col-12">

                        <div class="form-group" style="direction: rtl;">
                            <input type="email" name="email" style="text-align: right!important;height:55px!important" class="form-control" id="contact-email" placeholder="ایمیل" required oninvalid="this.setCustomValidity('لطفا ایمیل خود را وارد کنید')"
                                   oninput="setCustomValidity('')">

                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <select style="direction: rtl;height:55px!important" id="selectbasic" name="subject" class="form-control" required>
                                <option value="" disabled="" selected="" style="display: none;">نوع درخواست</option>
                                <option value="0">بالکن</option>
                                <option value="1">پاسیو</option>
                                <option value="2">بام</option>
                                <option value="3">دیوار</option>
                                <option value="4">فضای داخلی</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn alazea-btn mt-15" style="background-color: #577e3b" >ارسال</button>
                    </div>
                </div>
            </form>
        </div>
    </div>





<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>