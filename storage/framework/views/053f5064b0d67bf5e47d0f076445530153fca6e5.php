<?php $__env->startSection('title'); ?>
    <?php if($setting->about_title_seo==null): ?>
        <?php echo e($setting->title); ?>

    <?php else: ?>
        <?php echo e($setting->about_title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php echo e(@$setting->about_description_seo); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/images/setting/about_banner.png')); ?>);margin-top: 60px;">
            
        </div>

        <div class="container">
            
                
                    
                        
                            
                            
                        
                    
                
            
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### About Area Start ##### -->
    <section class="about-us-area">
        <div class="container"><br>
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> درباره ما </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-12 col-lg-5">
                    <!-- Section Heading -->

                    <!-- Progress Bar Content Area -->
                    
                        
                        
                            
                            
                                
                                    
                                
                                
                            
                        

                        
                        
                            
                            
                                
                                    
                                
                                
                            
                        

                        
                        
                            
                            
                                
                                    
                                
                                
                            
                        

                        
                        
                            
                            
                                
                                    
                                
                                
                            
                        
                    
                </div>

                
                    
                        
                            
                            
                                
                                    
                                    
                                    
                                
                            

                            
                            
                                
                                    
                                    
                                    
                                
                            

                            
                            
                                
                                    
                                    
                                    
                                
                            

                            
                            
                                
                                    
                                    
                                    
                                
                            
                        
                    
                
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                      <center> <h2>درباره ما</h2>   </center>

                    <h5><?php echo $setting->about_us; ?></h5>


                </div>
            </div>
        </div>
    </section>
    <!-- ##### About Area End ##### -->

    <!-- ##### Service Area Start ##### -->
    
        
            
                
                    
                    
                        
                        
                    
                
            

            
                
                    

                        
                        
                            
                            
                                
                            
                            
                            
                                
                                
                            
                        

                        
                        
                            
                            
                                
                            
                            
                            
                                
                                
                            
                        

                        
                        
                            
                            
                                
                            
                            
                            
                                
                                
                            
                        

                    
                

                
                    
                        
                        
                            
                        
                    
                
            
        
    
    <!-- ##### Service Area End ##### -->

    <!-- ##### Testimonial Area Start ##### -->
    
        
            
                
                    

                        
                        
                            
                                
                                    
                                        
                                    
                                
                                
                                    
                                        
                                        
                                            
                                            
                                        
                                        
                                        
                                            
                                            
                                        
                                    
                                
                            
                        

                        
                        
                            
                                
                                    
                                        
                                    
                                
                                
                                    
                                        
                                        
                                            
                                            
                                        
                                        
                                        
                                            
                                            
                                        
                                    
                                
                            
                        

                        
                        
                            
                                
                                    
                                        
                                    
                                
                                
                                    
                                        
                                        
                                            
                                            
                                        
                                        
                                        
                                            
                                            
                                        
                                    
                                
                            
                        

                    
                
            
        
    
    <!-- ##### Testimonial Area End ##### -->

    <!-- ##### Cool Facts Area Start ##### -->
    <section class="cool-facts-area bg-img section-padding-100-0" style="background-image: url(<?php echo e(url('assets/site/image/bg-img/cool-facts.png')); ?>);">
        <div class="container">
            <div class="row">

                <!-- Single Cool Facts Area -->

                <!-- Single Cool Facts Area -->


                <!-- Single Cool Facts Area -->
                <div class="col-10 col-sm-6 col-md-3" style="margin-left: 5%">
                    <div class="single-cool-fact d-flex align-items-center justify-content-center mb-100">
                        <div class="cf-icon">
                            <img src="<?php echo e(asset('assets/site/css/core-img/done.png')); ?>" alt="">
                        </div>
                        <div class="cf-content">
                            <h2><span class="counter">20</span></h2>
                            <h6>پروژه های انجام شده</h6>
                        </div>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-10 col-sm-6 col-md-3" style="margin-left: 6%">
                    <div class="single-cool-fact d-flex align-items-center justify-content-center mb-100">
                        <div class="cf-icon">
                            <img src="<?php echo e(asset('assets/site/css/core-img/doing.png')); ?>" alt="">
                        </div>
                        <div class="cf-content">
                            <h2><span class="counter">10</span>+</h2>
                            <h6>پروژه های در حال اجرا</h6>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-6 col-md-3" style="margin-left: 3%">
                    <div class="single-cool-fact d-flex align-items-center justify-content-center mb-100">
                        <div class="cf-icon">
                            <img src="<?php echo e(asset('assets/site/css/core-img/next.png')); ?>" alt="">
                        </div>
                        <div class="cf-content">
                            <h2><span class="counter">11</span></h2>
                            <h6>پروژه های پیش رو</h6>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-6 col-md-3">
                    <div class="single-cool-fact d-flex align-items-center justify-content-center mb-100">
                        <div class="cf-icon">
                            
                        </div>
                        <div class="cf-content">
                            
                            
                        </div>
                    </div>
                </div>


            </div>
        </div>
<br>
<br>
        <!-- Side Image -->
        <div class="side-img wow fadeInUp" data-wow-delay="500ms">
            <img src="<?php echo e(asset('assets/site/css/core-img/pot.png')); ?>" alt="">
        </div>
    </section>
    <!-- ##### Cool Facts Area End ##### -->

    <!-- ##### Team Area Start ##### -->
    
        
            
                
                    
                    
                        
                        
                    
                
            

            

                
                
                    
                        
                        
                            
                            
                            
                                
                                
                                
                                
                            
                        
                        
                        
                            
                            
                        
                    
                

                
                
                    
                        
                        
                            
                            
                            
                                
                                
                                
                                
                            
                        
                        
                        
                            
                            
                        
                    
                

                
                
                    
                        
                        
                            
                            
                            
                                
                                
                                
                                
                            
                        
                        
                        
                            
                            
                        
                    
                

                
                
                    
                        
                        
                            
                            
                            
                                
                                
                                
                                
                            
                        
                        
                        
                            
                            
                        
                    
                

            
        
    




<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>