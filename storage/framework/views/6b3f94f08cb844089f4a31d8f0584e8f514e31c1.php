
<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>داشبورد
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <h3>لیست </h3>
                    <form method="POST" action="<?php echo e(URL::action('Admin\PodController@postDelete')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        
                        <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید.');"
                                data-toggle="tooltip"
                                data-original-title="حذف موارد انتخابی"
                                class="btn btn-danger "><i class="fa fa-trash-o"></i> حذف انتخاب شده ها
                        </button>


                        <div class="table-responsive">

                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th>
                                        <center>
                                            <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>
                                        </center>
                                        
                                    </th>
                                    <th class="column-title" style="display: table-cell;">شماره</th>
                                    <th class="column-title" style="display: table-cell;">موضوع</th>
                                    <th class="column-title" style="display: table-cell;">تصویر</th>
                                    <th class="column-title" style="display: table-cell;">تاریخ ایجاد</th>
                                    <th class="column-title" style="display: table-cell;">عملیات</th>


                                </tr>
                                </thead>

                                <tbody>

                                <?php if(count($pod) == 0): ?>
                                    <h1> چیزی یافت نشد</h1>
                                <?php else: ?>
                                    <?php $__currentLoopData = $pod; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <center>
                                                    <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                                           type="checkbox"
                                                           value="<?php echo e($row->id); ?>"/>

                                                </center>
                                                
                                            </td>
                                            <td class=" "><?php echo e($key+1); ?></td>
                                            <td class=" "><?php echo e($row->title); ?></td>
                                            <td><img style="width: 100px " <?php if($row->image): ?>src="<?php echo e(asset('assets/images/pod/'.$row->image)); ?>"<?php else: ?> src="<?php echo e(asset('assets/site/image/bg-img/koko.jpg')); ?>"<?php endif; ?>><br></td>

                                            <td><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></td>
                                            <td><a href="<?php echo e(url('admin/pod/edit/'.$row->id)); ?>" class="btn btn-success">ویرایش</a></td>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>


                                </tbody>
                                <center> <?php if(count($pod)): ?> <?php echo $pod->appends(Request::except('page'))->render(); ?> <?php endif; ?></center>

                            </table>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>