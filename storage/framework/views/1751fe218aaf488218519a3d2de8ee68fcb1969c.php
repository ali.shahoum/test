<?php $__env->startSection('content'); ?>
    <div class="breadcrumb-area">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/images/pod/podcast_banner.png')); ?>);margin-top: 60px;">
            <h2></h2>
        </div>



    </div>

    <section class="alazea-blog-area section-padding-100-0">
        <div class="container">
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page">   پادکست ها  </li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row justify-content-center" style="text-align: center">

                <!-- Single Blog Post Area -->
                    <div class="container">
                        <div class="row">
                            <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12 text-center">
                                <!-- section-title -->
                                <div class="section-title">
                                    <h2></h2>
                                    <p></p>
                                </div>
                            </div>
                            <!-- /.section-title -->
                        </div>
                        <div class="row">
                            <?php $__currentLoopData = $pod; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <!-- video-testimonail -->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="video-testimonial-block">
                                    <div class="video-thumbnail"><img src="<?php echo e(asset('assets/images/pod/'.$row->image)); ?>" alt="" class="img-fluid"></div>
                                    <div class="video">
                                        <iframe src="<?php echo e(@$row->link); ?>" allowfullscreen>
                                        </iframe>
                                    </div>
                                    <a href="#" class="fa fa-play"></a>
                                </div>
                                <div class="video-testimonial-content">
                                    <h4 class="mb10"><?php echo e($row->title); ?></h4>
                                    <p></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- /.video-testimonail -->
                            <!-- video-testimonail -->

                            <!-- /.video-testimonail -->
                            <!-- video-testimonail -->

                            <!-- /.video-testimonail -->
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center mt-4">
                            </div></div>
                    </div>
                </div>

            </div>

    </section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>