<div class="col-md-3 left_col hidden-print">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            
        </div>

        <div class="clearfix">
        </div>

        <!-- menu profilex quick info -->
        <div class="profile clearfix">
            <div >
                

                
                <center><h3 style="color: #fff!important;">سلام <?php echo e($user->name); ?> <?php echo e($user->family); ?>  </h3></center>
            </div>
            <!--<div class="profile_info">-->


            <!--</div>-->
        </div>
        <!-- /menu profilex quick info -->

        <!--<br/>-->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                
                <ul class="nav side-menu">
                        <li><a href="<?php echo e(url('profile/info')); ?>"><i class="fa fa-users"></i> پروفایل کاربری </a></li>

                    <br>
                    <br>
                    <br>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="تنظیمات">
                <span class="glyphicon glyphicon-cog" aria-hidden="true" style="color: floralwhite"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: floralwhite"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true" style="color: floralwhite"></span>
            </a>
            <a data-toggle="tooltip" onclick="return confirm('آیا برای خروج مطمئن هستید.');" data-placement="top" title="خروج" href="<?php echo e(url('admin/logout')); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true" style="color: floralwhite"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
