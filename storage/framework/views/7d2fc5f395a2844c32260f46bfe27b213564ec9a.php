<?php $__env->startSection('content'); ?>
    <div class="right_col" role="main">

        <form method="POST" action="<?php echo e(url('admin/services/decor/add_balkon')); ?>" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
                <div class="panel panel-info">
                    <div class="panel-heading"><h2>ویرایش </h2></div>

                    <div class="panel-body">
                        <div class="col-md-11">

                            <label >موضوع</label>
                            <input  name="title" type="text"  class="form-control" value="<?php echo e($balkon->title); ?>">
                            <input  name="id" type="hidden"  class="form-control" value="<?php echo e($balkon->id); ?>">
                            <label >توضیحات</label>
                            <textarea  name="description" type="text"  class="form-control ckeditor"><?php echo e($balkon->description); ?></textarea>

                            <hr>
                            <label >موضوع سئو</label>
                            <input  name="title_seo" type="text"  class="form-control" value="<?php echo e($balkon->title_seo); ?>"><br>

                            <label > توضیحات سئو</label>
                            <textarea  name="description_seo" type="text"  class="form-control "><?php echo e($balkon->description_seo); ?></textarea><br>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <br><button id="singlebutton" name="singlebutton" class="btn btn-primary">ارسال</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>