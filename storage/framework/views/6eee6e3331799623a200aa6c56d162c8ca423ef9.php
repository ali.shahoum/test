<?php $__env->startSection('title'); ?>
    <?php if($article->title_seo==null): ?>
        <?php echo e($article->title); ?>

    <?php else: ?>
        <?php echo e($article->title_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description_seo'); ?>
    <?php if($article->description_seo == null): ?>
        <?php echo e($article->description); ?>

    <?php else: ?>
        <?php echo e($article->description_seo); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="breadcrumb-area" style="font-family: iran-sans!important;">
        <!-- Top Breadcrumb Area -->
        <div class="top-breadcrumb-area bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(<?php echo e(url('assets/site/image/bg-img/art.png')); ?>);margin-top: 60px;">

        </div>

        <div class="container">

        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <br><br>
    <!-- ##### Blog Content Area Start ##### -->
    <section class="blog-content-area section-padding-0-100" style="text-align: right;direction: rtl">
        <div class="container">
            <div class="row" style="direction: rtl">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e('/home'); ?>"><i class="fa fa-home"></i> خانه</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo e('/article'); ?>">  مقالات </a></li>
                            <li class="breadcrumb-item active" aria-current="page">   <?php echo e($article->title); ?>  </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- Blog Posts Area -->
                <div class="col-12 col-md-8" style="text-align: justify">
                    <div class="blog-posts-area">
                        <div class="single-widget-area search-div" style="direction: rtl;margin-left: 5%">
                            <form method="get"  class="search-form" action="<?php echo e(URL::action('Site\SiteController@getSearch')); ?>" >
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" name="search" value="search" >

                                <button type="submit" style="right: 280px;background-color:#f0f0f0!important;"> <i class="icon_search"></i></button>
                                <input type="text" name="title" id="widgetSearch" placeholder="جستجوی مقاله" required>
                            </form>
                        </div>
                        <!-- Post Details Area -->
                        <div class="single-post-details-area">
                            <div class="post-content" style="font-family: iran-sans!important;">
                                <h4 class="post-title"><?php echo e($article->title); ?></h4>
                                <div class="post mb-30">
                                    <a href="#"><i class="fa fa-clock-o" aria-hidden="true"><?php echo e(jdate('Y/m/d H:i',$article->updated_at->timestamp)); ?></i> </a>
                                    
                                </div>
                                <div class="article-detail2">
                                    <div class="post-thumbnail mb-30 ">
                                        <img src="<?php echo e(asset('assets/images/article/'.$article->image)); ?>" alt="">
                                    </div>
                                    <p style="font-family: iran-sans!important;"><?php echo $article->description_first; ?></p>

                                </div>

                                <div class="row article-detail">
                                    <div class="col-6">
                                        <img src="<?php echo e(asset('assets/images/article/'.$article->image)); ?>" alt="">
                                    </div>
                                    <div class="col-6" >
                                    <p style="font-family: iran-sans;  line-height: normal;!important;"><?php echo $article->description_first; ?></p>
                                    </div>

                                </div>
                                <p style="font-family: iran-sans!important;"><?php echo $article->description; ?></p>

                            </div>
                        </div>


                    </div>
                </div>

                <!-- Blog Sidebar Area -->
                <div class="col-12 col-sm-9 col-md-4 ">

                    <div class="post-sidebar-area ">

                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area search-div2" style="direction: rtl;margin-left: 5%">
                            <form method="get"  class="search-form" action="<?php echo e(URL::action('Site\SiteController@getSearch')); ?>" >
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" name="search" value="search" >

                                <button type="submit" style="right: 280px;background-color:#f0f0f0!important;"> <i class="icon_search"></i></button>
                                <input type="text" name="title" id="widgetSearch" placeholder="جستجوی مقاله" required>
                            </form>
                        </div>


                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h4>اخرین مقالات</h4>
                            </div>

                            <?php $__currentLoopData = $latest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="single-latest-post d-flex align-items-center">
                                    <div class="post-thumb">
                                        <img src="<?php echo e(asset('assets/images/article/'.$row->image)); ?>" alt="" style="width: 60px;height: 50px">
                                    </div>
                                    <div class="post-content">
                                        <a href="<?php echo e(url('/article_detail/'.$row->title_id)); ?>" class="post-title" style="text-align: center!important;">
                                            <h6><?php echo e($row->title); ?></h6>
                                        </a>
                                        <a href="#" class="post-date" style="text-align: left!important;"><?php echo e(jdate('Y/m/d H:i',$row->created_at->timestamp)); ?></a>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>


                    </div>
                </div>
            </div>

        </div>
    </section>

    <?php if(count($pod)>=5): ?>

        <div class="container" style="padding-bottom: 10px ;">
       <center><h2>اخرین پادکست ها</h2></center>
        <section class="customer-logos slider">
            <?php $__currentLoopData = $pod; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <div class="slide">
                <img src="<?php echo e(asset('assets/images/pod/'.$row->image)); ?>" alt="" class="image" style="height: 150px"/>
                <div class="overlay">
                    <div class="text">
                       <a  href="<?php echo e(@$row->link); ?>" style="color: #ffffff"> <h5><?php echo e($row->title); ?></h5></a></div>
                </div>
            </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </section>

        
    </div>
<?php endif; ?>
    <!-- ##### Blog Content Area End ##### -->

<?php $__env->stopSection(); ?>
<style>
    .slide {
        position: relative;
        width: 50%;
        /*height: 0px;*/
    }

    .image {
        display: block;
        width: 100%;
        height: auto;
    }

    .overlay {
        position: absolute;
        bottom: 100%;
        left: 0;
        right: 0;
        background-color: #596446;
        opacity: 55%;
        overflow: hidden;
        width: 100%;
        height:0;
        transition: .5s ease;
    }

    .slide:hover .overlay {
        bottom: 0;
        height: 100%;
    }

    .text {
        color: white;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }
</style>

<?php echo $__env->make('layout.site.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>